/* global AppConfig */
var kue = require('schejulekue')
  , jobs = kue.createQueue()
  , jade = require('jade')
  , _ = require("lodash")
  , redis = require('redis')
  , Services = require('../server/service')
  , when = require('when')
  , geolib = require('geolib')
  , fs = require('fs')
  , nodefn = require('when/node')
  , path = require('path');

function parseTempalate(model, template) {
  var templatePath = path.join(AppConfig.root, 'smstemplates', template + '.dash');
  return nodefn.call(fs.readFile.bind(fs), templatePath).then(function(t) {
    return _.template(t, model);
  })
}

jobs.process('reservation', function(job, done){
  var reservation = job.data.model;
  Services.reservation.getCoordinates(reservation._id)
    .then(function(coords) {
      Services.provider.nearCoordinates(coords.longitude, coords.latitude)
        .then(function(providers) {
          providers.forEach(function(provider) {
            var dist = geolib.getDistance(
                {latitude: provider.latitude, longitude: provider.longitude},
                {latitude: coords.latitude, longitude: coords.longitude}
            );

            parseTempalate({
              reservation: reservation,
              provider: provider
            }, 'reservationnotification').then(function(text) {
              return Services.twilio.sendSMS(text, provider.user.phone);
            }, function() {
              done(new Error(JSON.stringify(arguments[0])));
            }).catch(function() {
              done(new Error(JSON.stringify(arguments[0])));
            })

          });
        }).then(function() {done();});
    }, function() {
      done(new Error(JSON.stringify(arguments[0])));
    }).catch(function() {
      console.log('reservation failer' + JSON.strigify(arguments))
      done(new Error(JSON.stringify(arguments[0])));
    });
});
