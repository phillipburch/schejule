var mongoose = require('mongoose'),
  fs = require('fs');

function loadWorkers() {
  var basePath = __dirname;

  fs.readdirSync(basePath).forEach(function (file) {
    if (file !== 'index.js') {
      require(basePath+'/'+file);
    }
  });
  return this;
}

module.exports = {
  loadWorkers: loadWorkers
};
