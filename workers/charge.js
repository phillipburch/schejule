/* global AppConfig */
var kue = require('schejulekue')
  , mongoose = require('mongoose')
  , jobs = kue.createQueue()
  , redis = require('redis')
  , Services = require('../server/service')
  , Reservation = mongoose.model('Reservation')
  , when = require('when')
  , nodefn = require('when/node')
  , path = require('path');

jobs.process('charge', function(job, done) {
  var reservation = job.data.model;
  when(Reservation.findOne({_id: reservation.id}).populate('user').exec())
    .then(function(reservation) {
      // Need to get customer id
      return Services.stripe.charge(3000, reservation.user.customerId).then(function() {
        reservation.paid = true;
        return reservation.pSave();
      });
    })
});
