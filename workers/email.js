/* global AppConfig */
var kue = require('schejulekue')
  , jobs = kue.createQueue()
  , nodemailer = require('nodemailer')
  , jade = require('jade')
  , redis = require('redis')
  , mongoose = require('mongoose')
  , EmailLog = mongoose.model('EmailLog')
  , when = require('when')
  , nodefn = require('when/node')
  , path = require('path');


function parseTemplate ( model, template) {
  model.rootUrl = AppConfig.rootUrl;
  var templatePath = path.join(AppConfig.root, 'mailtemplates', template + '.jade');
  return nodefn.call(jade.renderFile.bind(jade), templatePath, model);
}

function canEmail(email, cb) {
  var client = redis.createClient();
  return nodefn.call(client.sismember.bind(client), 'unsubscribed:email', email)
    .then(function(response) {
      if(!!response) return when.reject('Can\'t email this person.');
    });
}

function getTransport () {
  return nodemailer.createTransport('SMTP' ,{
    service: 'Mandrill',
    auth: {
      user: AppConfig.mandrill.username,
      pass: AppConfig.mandrill.password
    }
  });
}

jobs.process('email', function(job, done){
  canEmail(job.data.to).then(function() {
    parseTemplate(job.data.model, job.data.template).then(function(body) {
      var transport = getTransport();
      return nodefn.call(transport.sendMail.bind(nodemailer), {
        from: job.data.from,
        to: (AppConfig.emailDisabled) ? 'phillipgburch@gmail.com' : job.data.to,
        subject: job.data.subject,
        html: body,
        generateTextFromHTML: true
      }).then(function() {
        done();
      }, function(err) {
        done(new Error(JSON.stringify(err)));
      });
    }, function(err) {
      done(new Error(JSON.stringify(err)));
    });
  }, function(err) {
    done(new Error(JSON.stringify(err)));
  })
});
