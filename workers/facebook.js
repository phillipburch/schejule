// TODO: Create a worker to get all facebook friends and stick them in database.

/* global AppConfig */
var kue = require('schejulekue')
  , mongoose = require('mongoose')
  , User = mongoose.model('User')
  , jobs = kue.createQueue()
  , redis = require('redis')
  , Services = require('../server/service')
  , Reservation = mongoose.model('Reservation')
  , w = require('when')
  , request = require("request")
  , nodefn = require('when/node')
  , path = require('path');

var getRequest = function(user, nextUrl, cb) {
  var url = 'https://graph.facebook.com/me/friends?limit=100&access_token=' + user.facebookToken;
  if(nextUrl) url = nextUrl;

  request(url , function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var resp = JSON.parse(body);
      Services.friend.addFacebook(user._id, resp.data).then(function() {
        if(resp.paging.next) return getRequest(user, resp.paging.next, cb);
        cb();
      }, function(err) {
        logger.error(err)
        cb(err)
      });
    } else {
      cb(err)
    }
  });
}

jobs.process('facebook:friends', function(job, done) {
  var user = job.data.model;

  Services.user.getUser(user._id).then(function(user) {
    // TODO: April 2015, this will no longer get a full list of friends. Per v2
    getRequest(user, null, function(err, next) {
      if(err) done(err);
      done()
    })
  });
});
