var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Provider = mongoose.model('Provider'),
  jwt = require('jwt-simple'),
  when = require('when');

module.exports = {
  getUser: function (id, fields) {
    return when(User.findOne({_id: id}, fields).exec());
  },

  addToken: function(id) {
    return User.findOne({_id: id}).exec()
      .then(function(user) {
        var data = {email: user.email, _id: user._id};
        var secret = 'super_secret_token';
        var token = jwt.encode(data, secret);
        return token;
      });
  },

  patch: function(criteria, values) {
    return when(User.update(criteria, { $set: values }).exec());
  },

  getByToken: function(token) {
    return when(User.findOne({token: token}).exec());
  },

  create: function(model) {
    return when(User.create(model)).then(function(user) {
      if(!user.isProvider)
        return user;

      // If isProvider, create a provider record.
      return when(Provider.create({
        name: user.name,
        user: user._id
      })).then(function() {return user; });
    }, function(err) {
      var errors = [];
      if(err && err.code === 11000) {
        // TODO: This could also be duplicate phone number
        errors.push({message: 'Email address already in use.'});
      } else if(err.name === 'ValidationError') {
        for(var m in err.errors) {
          errors.push(err.errors[m]);
        }
      }
      err.errors = errors;
      return when.reject(err);
    });
  },

  find: function(fields) {
    return when(User.find({}, fields).exec());
  }
};
