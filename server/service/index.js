module.exports = {
  user: require('./user'),
  friend: require('./friend'),
  email: require('./email'),
  stripe: require('./stripe'),
  twilio: require('./twilio'),
  provider: require('./provider'),
  inventory: require('./inventory'),
  reservation: require('./reservation')
};
