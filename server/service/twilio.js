/* global AppConfig */
var redis = require('redis'),
  nodefn = require('when/node'),
  twilio = require('twilio');

var service = {
  makeShortCode: function(length, phone, cb) {
    var client = twilio(AppConfig.twilio.sid, AppConfig.twilio.authToken);
    var redisClient  = redis.createClient();
    var me = this;
    var code = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    for( var i=0; i < length; i++ )
      code += possible.charAt(Math.floor(Math.random() * possible.length));

    redisClient.hsetnx('register:codes', code, phone, function(err, response) {
      if(response)
        cb(code);
      else
        me.makeShortCode(length, cb);
    });
  },

  sendSMS: function(message, to) {
    var client = twilio(AppConfig.twilio.sid, AppConfig.twilio.authToken);
    return nodefn.call(client.sendSms, {
        to: to,
        from: AppConfig.twilio.phone,
        body: message
      });
  },

  verifySMS: function(number, cb) {
    var client = twilio(AppConfig.twilio.sid, AppConfig.twilio.authToken);
    var me = this;
    me.makeShortCode(4, number, function(code) {
      client.sendSms({
        to: number,
        from: AppConfig.twilio.phone,
        body: 'Your LastVine code is ' + code + '. You can also tap this link: lastvine.com/' + code
      }, function(err, responseData) {
        cb(responseData);
      });
    });
  },

  call: function(number, cb) {
    var client = twilio(AppConfig.twilio.sid, AppConfig.twilio.authToken);
    client.makeCall({
      to: number,
      from: AppConfig.twilio.phone,
      url: 'http://www.example.com/twiml.php'
    }, function(err, responseData) {
      cb(responseData);
    });
  }

};

// Make class a singleton
module.exports = service;
