var mongoose = require('mongoose'),
  Friend = mongoose.model('Friend'),
  when = require('when');

module.exports = {
  findById: function(userId) {
    return when (Friend.findOne({user: userId}).exec())
  },

  addFacebook: function(id, friends) {
    return Friend.findOne({user: id}).exec(function(err, friend) {
      if(friend) {
        friend.facebook = friend.facebook.concat(friends);
        return when.promise(function  (resolve, reject) {
          friend.save(function() {
            resolve();
          });
        })
      } else {
        return Friend.create({
          user: id,
          facebook: friends
        });
      }

    })
  }
};
