var mongoose = require('mongoose'),
  Provider = mongoose.model('Provider'),
  when = require('when');

module.exports = {

  findOne: function (id, fields) {
    return when(Provider.findOne({_id: id}, fields).exec());
  },

  create: function(name, image, user) {
    console.log(arguments)
    return when(Provider.findOne({user: user}).exec())
      .then(function(provider) {
        if(provider)
          return when.reject({error: "You're aleardy a provider"});
      }).then(function() {
        return when(Provider.create({
          name: name,
          image: image,
          user: user
        }));
      });
  },

  // Updates happen on user id not provider id.
  update: function(id, values) {
    return when(Provider.update({user: id}, values, {multi: true}).exec());
  },

  nearCoordinates: function(longitude, latitude, miles) {
    var maxDistance = (miles || 10) / 0.000621371;
    var locationSettings = {};

    if(longitude && latitude) {
      locationSettings = {
        loc: {
          $near: {
            $geometry : {
              type : 'Point' ,
              coordinates:[longitude, latitude]
            },
            $maxDistance: maxDistance
          }
        }
      };
    }

    return when(this.find(locationSettings))
      .then(null, function() {console.log(arguments);});
  },

  find: function(filter, fields) {
    return when(Provider.find(filter, fields)
      .lean()
      .populate('user').exec()
      .then(function(services) {
        services.forEach(function(s, idx) {
          if(s.loc && s.loc.length === 2) {
            services[idx].longitude = s.loc[0];
            services[idx].latitude = s.loc[1];
          }
          delete s.loc;
        });
        return services;
      }
    ));
  },

  remove: function(id) {
    return when(Provider.remove({_id: id}).exec());
  }

};
