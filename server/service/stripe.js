
/*
 * GET home page.
 */
var config = require('../config'),
  w = require('when')
	client = require('stripe')(config.stripe.secret);

var service = {
	createCustomer: function(token) {
    return w.promise(function(resolve, reject) {
  		client.customers.create(
  		   { card: token },
  		   function(err, customer) {
		      if (err) {
		         reject(err.message);
		         return;
  		    }
  		    resolve(customer)
  		   }
  		 );
    });
	},

	charge: function(amount, customer) {
    return w.promise(function(resolve, reject) {
  		client.charges.create({
  			amount: amount,
  			customer: customer,
  			currency: 'usd'
  		}, function(err, responseData) {
  			if (err) {
  		    reject(err.message);
  		  }
  		  resolve(responseData)
  		});
    });
	},

	refund: function(id, amount, cb) {
		client.charges.refund(id, amount, function(err, responseData) {
			console.log(responseData)
			if (err) {
		        console.log(err.message);
		        return;
		    }
		   cb(responseData)
		});
	}

};

// Make class a singleton
module.exports = service;
