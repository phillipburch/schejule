var mongoose = require('mongoose'),
  Reservation = mongoose.model('Reservation'),
  stripeService = require('./stripe'),
  schejulers = require('../../schejulers'),
  kue = require('kue'),
  _ = require('lodash'),
  jobs = kue.createQueue(),
  jwt = require('jwt-simple'),
  when = require('when'),
  nodefn = require('when/node');

var MYSERVICE = {

  getReservation: function (id, fields) {
    return when(Reservation.findOne({_id: id}, fields).populate('user', 'name').exec());
  },

  getCoordinates: function(id) {
    return this.getReservation(id).then(function(reservation) {
      var loc = reservation.loc;
      return {longitude: loc[0], latitude: loc[1]};
    });
  },

  create: function(model) {
    model.loc = [model.longitude, model.latitude];
    return when(Reservation.create(model)).then(function(model) {
      schejulers.reservation.new(model);
      return model;
    }, function(err) {
      var errors = [];
      if(err.name === 'ValidationError') {
        for(var m in err.errors) {
          errors.push(err.errors[m]);
        }
        errors = errors;
      }

      return when.reject((errors.length) ? errors : err);
    }).catch(function() {
      console.log(arguments);
      when.reject(["unspecified error"]);
    });
  },

  find: function(filter, fields) {
    filter = _.defaults(filter, {
      cancelled: {$ne: true}
    });

    return when(Reservation.find(filter, fields)
      .populate('user', 'email').lean().exec()
      .then(function(services) {
        services.forEach(function(s, idx) {
          if(s.loc && s.loc.length === 2) {
            services[idx].longitude = s.loc[0];
            services[idx].latitude = s.loc[1];
          }
          delete s.loc;
        });
        return services;
      }
    ));
  },

  claim: function(reservationId, userId) {
    return this.getReservation(reservationId)
      .then(function(reservation) {
        reservation.provider = userId;
        reservation.claimed = true;
        return reservation.save();
      }).then(function() {
        // Need to send push notification to user with ETA
        return {success: true};
      }, function() {
        return when.reject({
          message: 'There was an error claiming this reservation'
        });
      });
  },

  //TODO: Verify that userId can cancel
  cancel: function(reservationId, userId) {
    return this.getReservation(reservationId)
      .then(function(reservation) {
        reservation.cancelled = true;
        return reservation.pSave();
      });
  },

  complete: function(id, carImage) {
    return this.getReservation(id)
      .then(function(reservation) {
        reservation.completed = true;
        if(carImage)
          reservation.completedImage= carImage;
        return reservation.pSave();
      });
  },

  // TODO: Verify user is in the reservation
  charge: function(id, customerId) {
    return this.getReservation(id).then(function(reservation) {
      return stripeService.charge(3000, customerId).then(function() {
        reservation.paid = true;
        return reservation.pSave();
      });
    })

    .then(function(reservation) {
      jobs.create('email', {
        title: 'Thanks for schejuling your Appointment',
        subject: 'Carwash schejuled',
        to: 'Phillipgburch@gmail.com',
        from: 'phillip@lastvine.com',
        template: 'newReservation',
        model: {}
      }).attempts(5).save();

      jobs.create('reservation', {
        title: 'Reservation for ' + reservation.user.name,
        model: reservation
      }).save();
    });
  },

  remove: function(id) {
    return when(Reservation.remove({_id: id}).exec());
  }
};

module.exports = MYSERVICE;
