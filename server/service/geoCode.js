var http = require('http');

var service = {

	geoCode: function(options, callback) {
		var  basePath = "/maps/api/geocode/json?sensor=false";
        var path = basePath + "&address=" + encodeURIComponent(options.address);

		var options = {
		  host: 'maps.googleapis.com',
		  path: path
		};

		http.request(options, function(response) {
		  var data = '';

		  // keep track of the data you receive
		  response.on('data', function(d) {
		    data += d;
		  });

		  response.on('end', function() {
		  	if(callback) callback(JSON.parse(data).results);
		  });
		}).end();
	}
};

module.exports = service;