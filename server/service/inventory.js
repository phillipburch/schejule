var mongoose = require('mongoose'),
  Provider = mongoose.model('Provider'),
  when = require('when');

module.exports = {
  find: function(provider, skip, take) {
    return when (Provider.find({_id: provider},
      {name: 1, inventory: {$slice: [skip, take]}}).exec())
  },

  findOne: function(id) {
    // TODO: Consider passing in provider id
    return when (Provider.findOne({"inventorys._id": id},
      {_id: 0, "inventorys.$": 1}).exec())
  },

  create: function(provider, item) {
    return when(Provider.findOneAndUpdate({_id: provider},
        {$addToSet: {inventorys: item}},
        {select: {inventorys: {$elemMatch: item}}
      }).exec());
  },

  update: function(id, values) {
    return when(Provider.findOneAndUpdate({
      'inventorys.id': id
    }, values, {select : {inventorys: {$elemMatch: item}}}).exec());
  },

  remove: function(providerId, inventoryId) {
    return when(Provider.update({_id: providerId})
      .update({$pull: {inventorys: {_id: inventoryId}}}).exec());
  }
};
