var should = require('should'),
  mongoose = require('mongoose'),
  when = require('when'),
  sinon = require('sinon'),
  service,
  userController;

beforeEach(function() {
  // Load Models
  require('../models').loadModels();
  service = require('../service');
  // load user
  userController = require('../api/user');
});

describe('User API Controller',function(){
  describe('core', function() {
    it('should instantiate',function(){
      userController.should.be.defined;
    });
  })

  describe('get list', function() {

    it('should call user service to get users',function(){
      service.user.find = sinon.stub().returns(when.promise());
      userController.list();
      service.user.find.called.should.be.true;
    });

    it('should return users in model property',function(done){
      service.user.find = sinon.stub().returns(when({
        test: 'test'
      }));

      userController.list().then(function(list) {
        list.should.be.eql({ model: { test: 'test' } });
        done();
      }, done).done();
    });

    it('should return error if user list fails from database',function(done) {
      service.user.find = sinon.stub().returns(when.reject());
      var rejectionCb = sinon.stub();
      userController.list().then(null, rejectionCb).then(function() {
        rejectionCb.called.should.be.true
        done();
      }).done()
    });
  });

  describe('settings', function() {
    it('should return model with user', function(done) {
      var context = {user: {name: 'Phillip'}};
      userController.settings.call(context).then(function(resp) {
        resp.model.should.be.eql({ item: context.user});
        done();
      }).done();
    })

    it('should throw error if not authenticated', function(done) {
      var rejectionCb = sinon.stub();
      userController.settings.call().then(null, rejectionCb).then(function() {
        rejectionCb.called.should.be.true
        done();
      }).done()
    })
  })
});

afterEach(function() {
  mongoose.models = {};
  mongoose.modelSchemas = {};
})
