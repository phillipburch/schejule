/**
 * Module dependencies.
 */

var express = require('express'),
  http = require('http'),
  config = require('./config'),
  port = process.env.SCHEJULEPORT || 3010,
  models = require('./models').loadModels(),
  middleware = require('./middleware'),
  errorHandler = require('errorhandler'),
  colors = require('colors'),
  kue = require('schejulekue'),
  jobs = kue.createQueue(),
  sugar = require('sugar'),
  cluster = require('cluster'),
  workers = require('../workers').loadWorkers(),
  routes = require('./routes'),
  kueLoaded,
  path = require('path');

GLOBAL.logger = require('./logger').create()
GLOBAL.AppConfig = config;

if (cluster.isMaster && process.env.NODE_ENV && process.env.NODE_ENV.toLowerCase() !== 'development') {
  var cpuCount = require('os').cpus().length;

  for (var i = 0; i < cpuCount; i += 1) {
    cluster.fork();
  }
} else {
  // Bootstrap db connection
  var app = express();
  models.connect(config);

  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');

  // Handle fileuploads. This needs to be one of the first thing to happen.
  app.use(middleware.fileupload.handleUploads);

  // Bootstrap routes
  app.use('/api', routes.api());
  app.use('/', routes.site());

  // Global error handler
  app.use(errorHandler());

  // Create server
  http.createServer(app).listen(port, function() {
    var portMsg = 'Server listening on port: ' + port + '.';
    var envMessage = 'App is currently running in '.yellow +
      process.env.NODE_ENV.bold.cyan +
      ' mode. To change this, update the NODE_ENVIRONMENT value in your bash profile.'.yellow;

    logger.info(portMsg.magenta);
    logger.info(envMessage);
  });

  process.on('uncaughtException', function (err) {
    logger.error(err);
  });

  if(!kueLoaded) {
    // Only load the kue once on master
    kue.app.listen(3011);
    jobs.promote();
    logger.info('kue loaded on 3011')
    kueLoaded = true;
  }
}

cluster.on('exit', function (worker) {
  logger.info('Worker ' + worker.id + ' died :(');
  cluster.fork();
});
