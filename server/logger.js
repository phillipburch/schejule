var winston = require('winston');

module.exports = {
  create: function () {
    var logger = new (winston.Logger)({
      transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
          filename: process.env.NODE_ENV + '.log'
        })
      ]
    });

    logger.handleExceptions(
      new winston.transports.File({
        filename: '/exceptions.log'
      })
    );
    return logger;
  }
}
