var _ = require('underscore');

// Config that is that same across all enviornments
var SharedConfig = {
  app: {
    name: 'Schejule'
  },
  twilio: {
      sid: 'ACc0b85689c06d40b0b999ae93c7f4141c'
    , authToken: "b5f9885a2f654b83d7602ba12b1a8a56"
    , phone: '+19728107229'
  },
  stripe: {
      secret: 'MWnUHy1gMZCguNi6BUXUF1mvgvpx9fcz'
  },
  bibleService: 'http://localhost:3160'
}

// Environment specific config
var Config = {
  development: {
    root: require('path').normalize(__dirname),
    rootUrl: 'http://192.168.1.129:' + process.env.NODEPORT,
    db: 'mongodb://localhost/schejule',
    emailDisabled: true,
    elasticSearch: {
        host: 'localhost',
        port: 9200
    },
    mandrill: {
      username: 'phillip.burch@live.com'
    , password: 'qurW-A3ZRKHpvXKDBnyVmw'
    },
    feedService: 'http://localhost:3000',
    twitter: {
        consumerKey: "an1owAmRYof1MqK2YduNNQ"
      , consumerSecret: "BoBcNIRs54bVvEjB8ZQyiZ90TmCJpI0lXaqBqUkHM"
      , callbackURL: 'http://localhost:' + process.env.NODEPORT + "/auth/twitter/callback"
    },
    facebook: {
        clientID: 187296184704119
      , clientSecret: "b81f9fcc45d0911038a0601dd2d7ebff"
      , callbackURL: 'http://localhost:3010/auth/facebook/callback'
    },
    redis: {
       port: '6379'
      , url: '127.0.0.1'
    }
  }

, test: {
    root: require('path').normalize(__dirname + '/../../app'),
    rootUrl: 'http://192.168.1.129:' + process.env.NODEPORT,
    db: 'mongodb://localhost/schejule',
    emailDisabled: false,
    elasticSearch: {
        host: 'localhost',
        port: 9200
    },
    mandrill: {
      username: 'phillip.burch@live.com'
    , password: 'tympxocYlDDIpCNv3sgxFA'
    },
    feedService: 'http://localhost:3000',
    twitter: {
        consumerKey: "an1owAmRYof1MqK2YduNNQ"
      , consumerSecret: "BoBcNIRs54bVvEjB8ZQyiZ90TmCJpI0lXaqBqUkHM"
      , callbackURL: 'http://localhost:' + process.env.NODEPORT + "/auth/twitter/callback"
    },
    facebook: {
        clientID: 187296184704119
      , clientSecret: "b81f9fcc45d0911038a0601dd2d7ebff"
      , callbackURL: 'http://localhost:3010/auth/facebook/callback'
    },
    redis: {
       port: '6379'
      , url: '127.0.0.1'
    }
  }
, production: {
    root: require('path').normalize(__dirname),
    rootUrl: 'http://www.schejule.com',
    emailDisabled: false,
    mandrill: {
      username: 'phillip.burch@live.com'
    , password: 'qurW-A3ZRKHpvXKDBnyVmw'
    },
    twitter: {
        consumerKey: "an1owAmRYof1MqK2YduNNQ"
      , consumerSecret: "BoBcNIRs54bVvEjB8ZQyiZ90TmCJpI0lXaqBqUkHM"
      , callbackURL: "http://72.2.113.133/auth/twitter/callback"
    },
    feedService: 'http://localhost:3000',
    elasticSearch: {
        host: 'localhost',
        port: 9200
    },
    facebook: {
        clientID: 187296184704119
      , clientSecret: "b81f9fcc45d0911038a0601dd2d7ebff"
      , callbackURL: "http://www.lastvine.com/auth/facebook/callback"
    },

    // This will like point to something else on another machine
    db: 'mongodb://phillip:pgbsr4@localhost:27017/schejule',
    // This will like point to something else on another machine
    redis: {
       port: '6379'
      , url: '127.0.0.1'
    }
  }
}

// Add Shared Config to the base config
for(var environment in Config) {
  _.extend(Config[environment], SharedConfig, false, false)
}


var environment = process.env.NODE_ENV || 'production';
module.exports = Config[environment.toLowerCase()];


