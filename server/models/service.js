module.exports = function (mongoose) {
  var Schema = mongoose.Schema,
    baseSchema = require('./base');

  var ServiceSchema = new Schema({
    name: String,
    price: String,
    user: {type: Schema.Types.ObjectId, ref: 'User' },
    loc: {type: [Number], index: '2dsphere'}
  });

  ServiceSchema.index({'location': '2dsphere'});
  ServiceSchema.plugin(baseSchema);

  mongoose.model('Service', ServiceSchema);
};
