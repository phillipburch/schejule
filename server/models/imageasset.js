module.exports = function (mongoose) {
    var Schema = mongoose.Schema
    , baseSchema = require('./base');

  var imageSchema = new Schema({
       _id: String
    ,  url: String
    , path: String
  });

  imageSchema.plugin(baseSchema);

  mongoose.model('ImageAsset', imageSchema);
};