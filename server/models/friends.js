module.exports = function (mongoose) {
    var Schema = mongoose.Schema
    , baseSchema = require('./base');

  var friendSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    facebook: [{id: String, name: String}],
  });

  friendSchema.plugin(baseSchema);

  mongoose.model('Friend', friendSchema);
};