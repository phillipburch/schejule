var mongoose = require('mongoose'),
  fs = require('fs');

function loadModels() {
  var modelsPath = __dirname;

  fs.readdirSync(modelsPath).forEach(function (file) {
    if (file !== 'base.js' && file !== 'index.js') {
      require(modelsPath+'/'+file)(mongoose);
    }
  });
  return this;
}

function connect(config) {
  console.log(config.db);
  return mongoose.connect(config.db);
}

module.exports = {
  loadModels: loadModels,
  connect: connect
};
