// TODO: May need to just merge this into user model.
module.exports = function (mongoose) {
  var Schema = mongoose.Schema,
    baseSchema = require('./base');

  var ProviderSchema = new Schema({
    type: String,
    averageDuration: Number,
    avgRating: Number,
    reviews: [{rating: Number, text: String, user: String, userId: String}],
    name: String,
    image: {type: String, default: 'http://www.placehold.it/300x300/EFEFEF/AAAAAA&text=no+image'},
    price: String,
    isAvailable: {type: String, default: false},
    user: {type: Schema.Types.ObjectId, ref: 'User' },
    loc: {type: [Number], index: '2dsphere'},
    inventorys: [{
      name: String,
      image: String,
      price: Number,
      quanity: Number
    }]
  });

  ProviderSchema.plugin(baseSchema);

  mongoose.model('Provider', ProviderSchema);
};

