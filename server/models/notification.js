module.exports = function (mongoose) {
    var Schema = mongoose.Schema
    , baseSchema = require('./base');

  var notificationSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    type: String,
    actor: { type: Schema.Types.ObjectId, ref: 'User' },
    verse: String,
    note: { type: Schema.Types.ObjectId, ref: 'Annotation' },
    group: { type: Schema.Types.ObjectId, ref: 'Group' }
  });

  notificationSchema.plugin(baseSchema);

  mongoose.model('Notification', notificationSchema);
};