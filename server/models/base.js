var nodefn = require('when/node');

module.exports = exports = function lastModifiedPlugin (schema) {
  schema.add({ lastMod: Date });

  schema.add({
    createdOn: {
      type: Date,
      default: Date.now
    }
  });

  schema.pre('save', function (next) {
    this.lastMod = new Date();
    next();
  });

  schema.method('pSave', function() {
    var self = this;
    return nodefn.call(self.save.bind(self))
      .then(function() {
        return self;
      });
  });
};
