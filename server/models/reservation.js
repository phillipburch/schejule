module.exports = function (mongoose) {
  var Schema = mongoose.Schema,
    baseSchema = require('./base');

  var ReservationSchema = new Schema({
    type: {type: String, default: 'basicCarWash'},
    provider: {type: Schema.Types.ObjectId, ref: 'User' },
    estimatedTime: Date,
    user: {type: Schema.Types.ObjectId, ref: 'User', required: 'User is required!' },
    loc: {type: [Number], index: '2dsphere'},
    address: {type: String, required: 'Address is required!' },
    carImage: String,
    status: String,
    paid: {type: Boolean, default: false},
    claimed: {type: Boolean, default: false},
    completed: {type: Boolean, default: false},
    completedImage: String,
    cancelled: {type: Boolean, default: false},
    license: String
  });

  ReservationSchema.plugin(baseSchema);
  mongoose.model('Reservation', ReservationSchema);
};
