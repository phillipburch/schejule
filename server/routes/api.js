var api = require('../api'),
  express = require('express'),
  expressLogger = require('morgan'),
  errorHandler = require('errorhandler'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),
  favicon = require('static-favicon'),
  cookieParser = require('cookie-parser'),
  bodyParser = require('body-parser'),
  router = express.Router();

module.exports = function () {
  router.use(expressLogger({
    format: 'dev',
    stream:{
      write: function(msg) {
        logger.info(msg.slice(0, -1))
      }
    }
  }));

  router.use(bodyParser.json());
  router.use(bodyParser.urlencoded());
  router.use(methodOverride());

  router.post('/user/create', api.decorate(api.auth.create));

  router.post('/phone/signup', api.decorate(api.auth.phoneCreate));
  router.post('/auth/register', api.decorate(api.auth.create));
  router.post('/auth/login', api.decorate(api.auth.login));
  router.get('/auth/logout', api.decorate(api.auth.logout));

  // User
  router.get('/user', api.decorate(api.user.list));
  router.get('/user/settings', api.decorate(api.user.settings));
  router.post('/user/image', api.user.uploadImage);

  router.route('/users/:id')
    .get(api.decorate(api.user.show))
    .patch(api.decorate(api.user.patch));

  router.post('/unsubscribe', api.decorate(api.user.saveUnsubscribe));

  // Uploads
  router.post('/asset/upload', api.imageAsset.upload);

  // Search
  router.get('/search', api.search.search);

  // Reservations
  router.route('/reservations')
    .get(api.decorate(api.reservation.list))
    .post(api.decorate(api.reservation.create));

  router.route('/customers')
    .post(api.decorate(api.customer.create));

  router.route('/email')
    .get(api.decorate(api.reservation.email));

  //Reservation actions
  router.get('/reservations/:id/claim', api.decorate(api.reservation.claim));
  router.post('/reservations/:id/complete', api.decorate(api.reservation.complete));
  router.post('/reservations/:id/charge', api.decorate(api.reservation.charge));
  router.post('/reservations/:id/cancel', api.decorate(api.reservation.cancel));

  // Service
  router.get('/providers', api.decorate(api.provider.list))
    .put('/providers/:id', api.decorate(api.provider.update))
    .get('/providers/:id', api.decorate(api.provider.findOne))
    .post('/providers', api.decorate(api.provider.create));

  router.get('/matches', api.decorate(api.match.list));

  router.delete('/providers/:id', api.decorate(api.provider.remove));
  router.post('/providers/me/location', api.decorate(api.provider.updateLocation));

  router.post('/providers/me/markavailable', api.decorate(api.provider.markAvailable));
  router.post('/providers/me/markunavailable', api.decorate(api.provider.markUnavailable));

  // Inventory
  router.get('/provider/:provider/inventory', api.decorate(api.inventory.list));
  router.post('/provider/:provider/inventory', api.decorate(api.inventory.create))

  router.get('/inventory/:id', api.decorate(api.inventory.show));
  router.put('/inventory/:id', api.decorate(api.inventory.update));
  router.post('/inventory', api.decorate(api.inventory.create));

  router.get('/email', api.decorate(api.reservation.email));

  router.get('/facebook/getFriends', api.decorate(api.social.getFBFriends));

  router.use(function(req, res, next) {
    res.status(404);
    // respond with json
    if (req.accepts('json')) {
      res.send({ error: 'Not found' });
      return;
    }

    // default to plain-text. send()
    res.type('txt').send('Not found');
  });
  return router;
};
