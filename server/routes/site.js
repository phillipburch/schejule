var site = require('../controllers'),
  passport = require('passport'),
  express = require('express'),
  middleware = require('../middleware'),
  router = express.Router(),
  expressLogger = require('morgan'),
  errorHandler = require('errorhandler'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),
  favicon = require('static-favicon'),
  cookieParser = require('cookie-parser'),
  path = require('path');

module.exports = function () {
  // Setup Middleware
  router.use(express.static(path.join(__dirname, '../public')));
  router.use(expressLogger({
    format: 'dev',
    stream:{
      write: function(msg) {
        logger.info(msg.slice(0, -1))
      }
    }
  }));
  router.use(bodyParser.json());
  router.use(bodyParser.urlencoded());
  router.use(methodOverride());
  router.use(favicon());
  router.use(cookieParser());

  middleware.passport();
   // use passport session
  router.use(passport.initialize());

  // Social
  router.get('/verify/facebook', passport.authenticate('facebook', {scope: ['public_profile', 'email', 'user_friends']}));
  router.get('/facebook', site.social.facebookRedirects );
  router.get('/facebook/getFriends', site.social.getFBFriends );
  router.get('/facebook/step2', site.social.step2 );
  router.get('/auth/facebook/callback',
    passport.authenticate('facebook', { successRedirect: '/verify/user/?facebookSuccess',
                                        failureRedirect: '/login' }));
  router.get('/auth/twitter/callback',
    passport.authenticate('twitter', { successRedirect: '/?twittersuccess=1',
                                       failureRedirect: '/login' }));

  //Auth
  router.get('*', site.decorate(site.home.home));

  router.use(function (req, res, next) {
    if ('/robots.txt' === req.url) {
      res.type('text/plain');
      res.send('User-agent: *\nDisallow: /');
    } else {
      next();
    }
  });

  router.use(function(req, res, next) {
    res.status(404);
    // respond with html page
    res.render('404', { url: req.url });
    return;
  });

  return router;
};
