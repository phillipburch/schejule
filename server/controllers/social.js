/*
 * GET home page.
 */
var mongoose = require('mongoose'),
  request = require("request"),
  w = require('when'),
  geoip = require('geoip-lite');

exports.facebookRedirects = function(req, res){
  if (!this.user) {
    res.redirect('/facebook/step2');
  } else {
    res.redirect('/');
  }
};

exports.getFBFriends = function (req, res) {
  var me = this;
  return w.promise(function(resolve, reject) {
    request('https://graph.facebook.com/me/friends?access_token=' +me.user.facebookToken , function (error, response, body) {
      if (!error && response.statusCode == 200) {
        return resolve(JSON.parse(body).data);
        //console.log(body) // Print the google web page.
      }
      reject(new Error(error))
    });
  });
};

exports.step2 = function(req, res) {
  res.render('social/step2', {
    user: req.session.fbProfile
  });
};
