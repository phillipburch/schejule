var mongoose = require('mongoose')
  , passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy
  , FacebookStrategy = require('passport-facebook').Strategy
  , TwitterStrategy = require('passport-twitter').Strategy
  , BearerStrategy = require('passport-http-bearer').Strategy
  , elasticSearchClient = require('elasticsearchclient')
  , Config = require('../config')
  , Services = require('../service')
  , User = mongoose.model('User')


module.exports = function () {
  // require('./initializer')
  // serialize sessions
  passport.serializeUser(function(user, done) {
    done(null, user.id)
  })

  passport.deserializeUser(function(id, done) {
    User.findOne({ _id: id }, function (err, user) {
      done(err, user)
    })
  })

  passport.use(new BearerStrategy(
    function(token, done) {
      User.findOne({ token: token }, function (err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false); }
        return done(null, user, { scope: 'all' });
      });
    }
  ));

  // use local strategy
  passport.use(new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password'
    },
    function(email, password, done) {
      User.findOne({ email: email.toLowerCase() }, function (err, user) {
        if (err) { return done(err) }
        if (!user) {
          return done(null, false, { message: 'Unknown user' })
        }
        if (!user.authenticate(password)) {
          return done(null, false, { message: 'Invalid password' })
        }
        return done(null, user)
      })
    }
  ))

  passport.use(new FacebookStrategy({
    clientID: AppConfig.facebook.clientID,
    clientSecret: AppConfig.facebook.clientSecret,
    callbackURL: AppConfig.facebook.callbackURL,
    passReqToCallback: true
    },
    function(req, accessToken, refreshToken, profile, done) {
      User.findOne({facebookId: profile.id}).exec(function(err, item) {
        if(item) {
          item.facebookId = profile.id;
          item.facebookToken = accessToken;
          item.provider = 'facebook';
          item.save(function(err, user) {
            Services.user.addToken(user._id).then(function(token) {
              return done(null, item);
            });
          })
        } else {
          if(profile.emails && profile.emails.length)
            var email = profile.emails[0].value;

          if(email) {
            Services.user.getByEmail(email).then(function(user) {
              if(user) {
                // TODO: Create a worker to get all facebook friends and stick them in database.
                user.facebookId = profile.id;
                user.facebookToken = accessToken;
                user.provider = 'facebook';
                user.save(function(err, user) {
                  Services.user.addToken(user._id).then(function(token) {
                    return done(null, item);
                  });
                })
                return;
              }

              Services.user.create({
                name: profile.displayName,
                facebookId: profile.id,
                email: email,
                facebookToken: accessToken,
                provider: 'facebook'
              }).then(function(user) {
                // TODO: Create a worker to get all facebook friends and stick them in database.
                Services.user.addToken(user._id).then(function(token) {
                  return done(null, user);
                });
              })
            });
          } else {
            // This user doesn't have an email, but maybe has phone number
            Services.user.create({
              name: profile.displayName,
              facebookId: profile.id,
              facebookToken: accessToken,
              provider: 'facebook'
            }).then(function(user) {
              // TODO: Create a worker to get all facebook friends and stick them in database.
              Services.user.addToken(user._id).then(function(token) {
                return done(null, user);
              });
            })
          }
        }
      })
    }
  ));

  passport.use(new TwitterStrategy({
      consumerKey: AppConfig.twitter.consumerKey,
      consumerSecret: AppConfig.twitter.consumerSecret,
      callbackURL: AppConfig.twitter.callbackURL,
      passReqToCallback: true
    },
    function(req, accessToken, refreshToken, profile, done) {
      // This needs to find or create a real user. Sorta like facebook
      User.findOne({twitterId: profile.id}).exec(function(err, item) {
        if(item) {
          console.log(item)
          done(null, item)
        } else {
          var user = new User({
            images: profile.photos,
            name: profile.displayName,
            twitterId: profile.id,
            twitterHandle: profile.username,
            location: profile._json.location,
            backgroundImage: profile._json.profile_background_image_url
          });
          user.save(function(err, item) {
            done(null, item);
          });
        }
      })
    }
  ));
};