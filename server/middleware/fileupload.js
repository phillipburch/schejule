//FileUpload Controller has no access to session and is simply a middleware
var multiparty = require('multiparty'),
  imageService = require('../service/image'),
  uuid = require('node-uuid'),
  mongoose = require('mongoose');

var RE_MIME = /^(?:multipart\/.+)$/i;

function isMultipart(req) {
  var str = req.headers['content-type'] || '';

  return RE_MIME.test(str.split(';')[0]);
}

var fileUpload =  {
  handleUploads: function(req, res, next) {
    var socket, files, imageId = uuid.v4(), fields = {};

    if (req.method === 'GET' || req.method === 'HEAD' || !isMultipart(req))
      return next();

    //if (req.url.indexOf('/api/reservations') === -1)
      //return next();
    //res.json({success: true, assetId: imageId});
    console.log('Uploading file....')
    var form = new multiparty.Form();

    form.uploadDir =  AppConfig.root + '/temp';
    console.log(form.uploadDir);

    // Parse the fields myself
    form
      .on('field', function(field, value) {
        fields[field] = value;
      });

    // I don't like the way "parsedFields" works it returns values as array
    form.parse(req, function(err, parsedFields, files) {

      var fileName = Object.keys(files)[0];
      var file = files[fileName];
      imageService.save(Date.create().format('{yyyy}{mm}{dd}'), imageId, 800, null, file[0], function(url) {
        fields[fileName] = url;
        req.body = fields;
        next();
      });
    });
  },

  initSocket: function(socket) {
    socket.on('registerUpload', function() {
      socket.emit('socketId', socket.id);
    });
  }
};

/*onIOInitiated(function() {
  io.sockets.on('connection', function (socket) {
    FileUpload.initSocket(socket);
  });
});*/

module.exports = fileUpload;
