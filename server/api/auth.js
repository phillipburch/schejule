
/*
 * GET users listing.
 */

var mongoose = require('mongoose'),
  w = require('when'),
  nodefn = require('when/node'),
  service = require('../service'),
  passport = require('passport'),
  uuid = require('node-uuid'),
  User = mongoose.model('User');

var controller = {
  phoneCreate: function(email, phone, password, isProvider) {
    var self = this;
    return service.user.create({
      email: email,
      phone: phone,
      password: password,
      provider: 'email',
      isProvider: isProvider
    }).then(function(newUser) {
      return service.user.addToken(newUser._id).then(function(token) {
        service.email.newUserReport(newUser);
        return {token: token};
      });
    }, function(err) {
      return {errors: err.errors || err};
    });
  },

  create: function(email, password, name) {
    var self = this;
    return service.user.create({
      email: email,
      password: password,
      name: name,
      provider: 'email'
    }).then(function(newUser) {
      service.email.newUserReport(newUser);
      return service.user.addToken(newUser._id).then(function(token) {
        return {template: '', model: newUser, token: token};
      });
    }, function(err) {
      return {errors: err.errors || err};
    });
  },

  login: function(next) {
    var self = this;

    return w.promise(function(resolve, reject) {
      passport.authenticate('local', function(err, user) {
        if(user) {
          service.user.addToken(user._id).then(function(token) {
            resolve({ token: token});
          });
        } else {
          return resolve({
            success: false,
            errors: ['Email or password is incorrect.']
          });
        }
      })(self.req, self.res);
    });
  },

  uploadImage: function(req, res) {
    var data = req.user;
    var imageId = 'user_' + req.user._id;
    service.image.save(imageId, uuid.v4(), 300, null, req.files.upload,
      function(url) {
        data.thumbnailUrl = url;
        data.save(function (err, item) {
          res.json(item.thumbnailUrl);
        });
      });
  }
};

module.exports = controller;
