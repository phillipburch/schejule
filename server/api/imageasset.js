var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  uuid = require('node-uuid'),
  imageService = require('../service/image'),
  imageAsset = mongoose.model('ImageAsset');

// No Reason to keep referential integrity here.
var controller = {
  upload: function(req, res) {
    var me= this;
    imageService.save(req.user._id, uuid.v4(), 800, null, req.files.mainImage, function(url) {
      var asset = new imageAsset({
        url: url
      });
      asset.save(function() {
        res.json({success: true, asset: asset});
      });
    });
  }
};

module.exports = controller;