var _ = require('underscore'),
  w = require('when'),
  jwt = require('jwt-simple'),
  services = require('../service'),
  decorate;

// get arguments that controller expects
function extractArgumentMap(functionCode) {
  var argumentStringMatch = functionCode.match(new RegExp('\\([^)]*\\)', ''));

  var argumentMap = argumentStringMatch[0].match(new RegExp('[^\\s,()]+', 'g'));

  if(!argumentMap || !argumentMap.length)
    return([]);

  return(argumentMap);
}

// Call Acutal controller method
function invoke (method, namedArguments, context) {
  return w.promise(function(resolve, reject) {
    var orderedArguments = [];

    if(!method) {
      return reject({
        errorCode: 404,
        message: 'Route not defined correctly'
      });
    }

    method.argumentMap = extractArgumentMap(method.toString());

    for (var i = 0 ; i < method.argumentMap.length ; i++){
      if (method.argumentMap[ i ] in namedArguments){
        orderedArguments.push(namedArguments[method.argumentMap[i]]);
      } else {
        orderedArguments.push(null);
      }
    }

    return resolve(method.apply(context, orderedArguments));
  });
}

// Find user based on auth token
function hydrateUser (token, res) {

  if(!token) // Some request don't require auth so just pass through
    return w();

  var tokenData;

  try {
    tokenData = jwt.decode(token, 'super_secret_token');
  } catch(e){
    // Somethings wrong with token
    return w.reject({errorCode: 401, error: 'Bad Access token'});
  }

  if(!tokenData || !tokenData._id)
    return w.reject({errorCode: 401, error: 'Bad Access token'});

  return services.user.getUser(tokenData._id).then(function(user) {
    if(user) {
      return user;
    } else {
      // It's a valid token, but the user is not in the database
      return w.reject({errorCode: 401, error: 'Bad Access token'});
    }
  }).catch(function() {
    // something unspecified error in get user
    return w.reject({errorCode: 401, error: 'Bad Access token'});
  });
}

// Main function. Decorates a controller method and injects values
decorate = function (method) {
  return function (req, res) {
    var token = req.body.token || req.query.token || req.headers['x-auth-token'];

    hydrateUser(token, res).then(function(user) {
      if(user)
        req.user = user;

      var options = _.extend(req.body, req.files, req.query, req.params),
        context = {
          req: req,
          res: res,
          user: req.user
        };

      invoke(method, options, context).then(function (result) {
        res.json(result);
      }, function (error) {
        logger.error(error);
        var errorCode = error.errorCode || 500;
        res.json(errorCode, error);
      }).catch(function(error) {
        logger.error(error)
        res.json('404');
      });
    }, function(error) {
      logger.error(arguments);
      var errorCode = error.errorCode || 500;
      res.json(errorCode, error);
    });
  };
};


module.exports = {
  auth: require('./auth'),
  customer: require('./customer'),
  search: require('./search'),
  provider: require('./provider'),
  inventory: require('./inventory'),
  match: require('./match'),
  reservation: require('./reservation'),
  imageAsset: require('./imageasset'),
  social: require('./social'),
  user: require('./user'),
  decorate: decorate
};
