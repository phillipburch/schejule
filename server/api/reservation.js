/*
 * GET users listing.
 */

var mongoose = require('mongoose'),
  w = require('when'),
  model = mongoose.model('Service'),
  nodefn = require('when/node'),
  Services = require('../service'),
  kue = require('kue'),
  jobs = kue.createQueue(),
  User = mongoose.model('User');

var controller = {
  // Because this is a multipart form, it can't send arguments as json
  create: function(latitude, longitude, carImage, license, address, reservation) {
    if(!this.user)
      return { success: false, errors: ['User not authorized']};

    var self = this;

    var myReservation = reservation || {
      latitude: latitude,
      longitude: longitude,
      carImage: carImage,
      license: license,
      address: address
    };

    myReservation.user = self.user._id;

    return Services.reservation.create(myReservation)
      .then(function(reservation) {
        reservation = reservation.toObject();
        reservation.user = { _id: self.user._id, email: self.user.email };
        return {reservation: reservation};
      }, function(err) {
        return {errors: err.errors || err};
      });
  },

  claim: function(id) {
    if(!this.user)
      return {success: false, errors: ['User not authorized']};

    return Services.reservation.claim(id, this.user._id);
  },

  cancel: function(id) {
    if(!this.user)
      return {success: false, errors: ['User not authorized']};

    return Services.reservation.cancel(id, this.user._id);
  },

  complete: function(id, carImage) {
    if(!this.user)
      return {success: false, errors: ['User not authorized']};

    return Services.reservation.complete(id, carImage).then(function(reservation) {
      return {reservation: reservation};
    });
  },

  charge: function(id, customerId) {
    if(!this.user)
      throw {success: false, errors: ['User not authorized']};

    // You need to be the customer
    if(this.user.customerId !== customerId)
      return {success: false, errorCode: 401};

    return Services.reservation.charge(id, customerId);
  },

  email: function() {
    jobs.create('email', {
      title: 'welcome email for phillip',
      subject: 'welcome email for Phillip',
      to: 'phillipgburch@gmail.com',
      from: 'phillipgburch@lastvine.com',
      template: 'simple_test',
      model: {}
    }).attempts(5).save();
  },

  list: function(longitude, latitude, miles, claimed) {
    if(!this.user)
      return {success: false, errors: ['User not authorized']};

    var filter = {};

    if(longitude && latitude) {
      filter.loc = {
        $near: {
          $geometry : {
            type : 'Point' ,
            coordinates:[longitude, latitude]
          },
          $maxDistance: (miles || 20) / 0.000621371
        }
      };
    }

    if(claimed)
      filter.claimed = claimed;

    return Services.reservation.find(filter).then(function(reservations) {
      return {reservations: reservations};
    }, function () {
      console.log(arguments);
    });
  }
};

module.exports = controller;
