/*
 * GET users listing.
 */

var mongoose = require('mongoose'),
  w = require('when'),
  model = mongoose.model('Service'),
  nodefn = require('when/node'),
  Services = require('../service'),
  User = mongoose.model('User');

var controller = {
  // Because this is a multipart form, it can't send arguments as json
  create: function(id, paymentToken) {
    if(!this.user)
      return { success: false, errors: ['User not authorized']};
    var user = this.user;

    return Services.stripe.createCustomer(paymentToken)
      .then(function(customer) {
        // May need to wait on this to finish...
        User.update({_id: user._id}, {customerId: customer.id}).exec();
        return customer;
      });
  }
};

module.exports = controller;
