
/*
 * GET users listing.
 */

var mongoose = require('mongoose')
  , redis = require('redis')
  , w = require('when')
  , nodefn = require('when/node')
  , uuid = require('node-uuid')
  , service = require('../service')
  , elasticSearchClient = require('elasticsearchclient')
  , validator = require('validator');

var controller = {
  list: function(){
    return service.user.find().then(function(users) {
      return {
        model: users
      };
    }, function() {
      return w.reject({errorCode: 500, message: 'Something went wrong.'});
    });
  },

  saveUnsubscribe: function(email) {
    var client = redis.createClient(),
      key = 'unsubscribed:email';

    return nodefn.call(client.sadd.bind(client), key, 'email')
      .then(function(response) {
        return {model: {success: !!response}};
      }, function(err) {
        return err;
      });
  },

  patch: function(id, values) {
    // If you're not the user, you can't update.
    if(!this.user || this.user._id.toString() !== id)
      return w.reject({
          errorCode: 401,
          message: 'You\'re unauthorized to do this'
        });

    return service.user.patch({_id: id}, values)
      .then(function(updateCount) {
        return {success: true};
      }, function(error) {
        return error;
      });
  },

  favorites: function(req, res) {
    var client = redis.createClient();
    var multi = client.multi();
    var followerCount, followingCount, isFollowee, me = this;
    service.user.getUser(req.params.id).exec(function(err, user) {
      multi.zcard('following:' + req.params.id, function(err, response) {
        followingCount = response;
      });
      multi.zcard('user:followers:' + req.params.id, function(err, response) {
        followerCount = response;
      });
      multi.zrank('user:followers:' + user._id, req.user._id, function(err, response) {
        isFollowee = Object.isNumber(response);
      });

      multi.exec(function() {
        respond(res, 'user/show', {
          item: user,
          followerCount: followerCount,
          followingCount: followingCount,
          tab: 'favorites',
          isFollowee: isFollowee,
          posts: []
        });
      });
    });
  },

  show: function(id) {
    var client = redis.createClient();
    var multi = client.multi();
    var followerCount, followingCount, me, isFollowee;

    return service.user.getUser(id).then(function(user) {
      return{ user: user };
    }, function(err) {
      return { error: err };
    });
  },

  uploadImage: function(req, res) {
    var data = req.user;
    var me = this;
    data.thumbnailUrl = req.body.thumbnailUrl;
    data.save(function (err, item) {
      res.json(item.thumbnailUrl);
    });
  },

  settings: function() {
    var self = this;
    if(!this.user)
      return w.reject({errorCode: 401, message: 'You have to be logged in'});

    return w({template: 'user/show', model: {
      item: self.user
    }});
  }
};

module.exports = controller;
