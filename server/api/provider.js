/*
 * GET users listing.
 */

var mongoose = require('mongoose'),
  w = require('when'),
  model = mongoose.model('Service'),
  nodefn = require('when/node'),
  Services = require('../service'),
  User = mongoose.model('User');

var controller = {
  list: function (longitude, latitude, miles) {
    var maxDistance = (miles || 10) / 0.000621371;

    return Services
      .provider
      .nearCoordinates(longitude, latitude, miles)
      .then(function(providers) {
        return {providers: providers};
      });
  },

  findOne: function (id) {
    return Services
      .provider
      .findOne(id)
      .then(function(provider) {
        return {provider: provider};
      });
  },

  updateLocation: function(longitude, latitude) {
    if(!this.user)
      return {success: false, errors: ['User not authorized']};

    return Services.provider.update(this.user._id, {
      loc: [longitude, latitude]
    }).then(function() { return {success: true }; });
  },

  create: function(name, image, provider) {
    if(!this.user)
      return {success: false, errors: ['User not authorized']};

    var self = this;

    if(provider){
      var name = name || provider.name;
      var image = image || provider.image;
    }

    return Services.provider.create(name, image, this.user._id)
      .then(function(provider) {
        provider.user = self.user._id;
        return {provider: provider};
      }, function(error) {
        error.errorCode = 422;
        return w.reject(error);
      });
  },

  markAvailable: function() {
    if(!this.user)
      return {success: false, errors: ['User not authorized']};

    return Services.provider.update(this.user._id, {isAvailable: true})
      .then(function() {
        return {success: true };
      });
  },

  markUnavailable: function() {
    if(!this.user)
      return {success: false, errors: ['User not authorized']};

    return Services.provider.update(this.user._id, {isAvailable: false})
      .then(function() {
        return {success: true };
      });
  },

  remove: function(id) {
    return Services.provider.remove(id)
      .then(function() {
        return null;
      }, function() {
        return {success: false};
      });
  }


};

module.exports = controller;
