var request = require("request"),
  w = require('when');

module.exports = {
  getFBFriends: function () {
    var me = this;
    if(!this.user) {
      return {success: false, errors: ['User not authorized']};
    }

    return w.promise(function(resolve, reject) {
      // TODO: April 2015, this will no longer get a full list of friends. Per v2
      request('https://graph.facebook.com/me/friends?limit=100&access_token=' +me.user.facebookToken , function (error, response, body) {
        if (!error && response.statusCode == 200) {
          // TODO: Create a worker to get all facebook friends and stick them in database.
          return resolve(JSON.parse(body).data);
        }

        reject(new Error(error))
      });
    });
  }
};

