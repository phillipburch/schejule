/*
 * GET users listing.
 */

var mongoose = require('mongoose'),
  w = require('when'),
  model = mongoose.model('Service'),
  nodefn = require('when/node'),
  Services = require('../service'),
  User = mongoose.model('User');

var controller = {
  list: function (provider, skip, take) {
    var skip = skip || 0,
      take = take || 10;

    return Services
      .inventory
      .find(provider, skip, take)
      .then(function(provider) {
        return {provider: provider};
      });
  },

  show: function(id) {
    return Services
      .inventory
      .findOne(id)
      .then(function(response) {
        if(response.inventorys && response.inventorys.length)
          return {inventory: response.inventorys[0]}

        return {inventory: {}};

      })
  },

  create: function(provider, name, price, image, quantity) {
    if(!this.user)
      return {success: false, errors: ['User not authorized']};

    var self = this;

    return Services.inventory.create(provider, {
      name: name,
      price: price,
      image: image,
      quantity: quantity
    }).then(function(provider) {
        return {provider: provider};
      }, function(error) {
        error.errorCode = 422;
        console.log(error)
        return w.reject(error);
      });
  },

  update: function(provider, values) {
    if(!this.user)
      return {success: false, errors: ['User not authorized']};

    var self = this;

    return Services.inventory.update(provider, values)
      .then(function(provider) {
        return {provider: provider};
      }, function(error) {
        error.errorCode = 422;
        return w.reject(error);
      });
  },

  remove: function(provider, inventory) {
    if(!this.user)
      return {success: false, errors: ['User not authorized']};

    var self = this;

    return Services.inventory.remove(provider, inventory)
      .then(function(provider) {
        return {provider: provider};
      }, function(error) {
        error.errorCode = 422;
        return w.reject(error);
      });
  }

};

module.exports = controller;
