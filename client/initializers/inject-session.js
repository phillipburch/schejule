Ember.Application.initializer({
  name: 'Session-injection',

  initialize: function(container, application) {
    App.register('session:current', App.Session, {singleton: true});
    App.inject('controller', 'session', 'session:current');
    App.inject('route', 'session', 'session:current');
  }
});