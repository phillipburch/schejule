App.UserEditController = Ember.ObjectController.extend({
  actions: {
    update: function () {
      var self = this;

      var url = '/api/users/' + this.get('id');

      $.ajax(url, {
        type: 'PATCH',
        data: JSON.stringify({values: {
          email: this.get('email'),
          name: this.get('name')
        }}),
        contentType: 'application/json',
        headers: {
          'x-auth-token': localStorage.getItem('token')
        }
      }).then(function() {
        self.transitionToRoute('/');
      });
    }
  }
});
