App.LoginController = Ember.ObjectController.extend({

  actions: {
    login: function () {
      var self = this;

      $.post('/api/auth/login', {
        email: this.get('model').email,
        password: this.get('model').password
      }).then(function(response) {
        if(response.token) {
          self.session.setToken(response.token);
          self.transitionToRoute('/');
        } else{
          self.set('errors', response.errors);
        }
      });
    }
  },

  isAuthenticated: function() {
    return this.get('session').isAuthenticated;
  }.property('session.isAuthenticated')

});
