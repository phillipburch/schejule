App.RegisterController = Ember.ObjectController.extend({

  // Define controller actions
  actions: {
    register: function () {
      var self = this;
      var model = this.get('model');

      $.post('/api/phone/signup', {
        phone: model.phone,
        email: model.email,
        password: model.password
      }).then(function(response) {
        if(response.token) {
          self.session.setToken(response.token);
          self.transitionToRoute(response.redirect);
        } else {
          var arr = $.map(response.errors, function(o) { return o.message; });
          self.set('errors', arr);
        }
      });

    }
  },

  isAuthenticated: function() {
    return this.get('session').isAuthenticated;
  }.property('session.isAuthenticated')
});
