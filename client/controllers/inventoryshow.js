App.InventoryShowController = Ember.ObjectController.extend({

  actions: {
    removeFirstItem: function() {
      this.objectAtContent(0).destroyRecord();
    }
  }
});
