Ember.Handlebars.helper('stringify', function(value, options) {
  var safeString = Ember.Handlebars.SafeString;
  var json = JSON.stringify(value, null, 2);
  return new safeString(json);
});
