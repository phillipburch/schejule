(function () {
  'use strict';

  App.Router.reopen({
    location: 'auto'
  });

  App.Router.map(function () {
    this.route('home', {path: '/'});
    this.route('register', {path: '/register'});
    this.route('search', {path: '/search'});
    this.route('login', {path: '/login'});

    this.resource('match', function() {
      this.route('new');
      this.route('list');
    });

    this.resource('user', function() {
      this.route('edit', { path: 'edit/:user_id' });
    });
  });

  App.UserEditRoute = Ember.Route.extend({
    model: function(params) {
      return this.store.find('user', params.user_id);
    }
  });

  App.MatchListRoute = Ember.Route.extend({
    model: function(params) {
      return this.store.find('match');
    }
  });

  App.UserRoute = Ember.Route.extend({
    model: function() {
      return {routeName: 'User Route'};
    }
  });

  App.LoginRoute = Ember.Route.extend({
    model: function() {
      return {};
    }
  });

  App.RegisterRoute = Ember.Route.extend({
    model: function() {
      return {};
    }
  });

  App.HomeRoute = Ember.Route.extend({
    model: function() {
      var self = this;
      // This could pause the loading of the page for a bit.
      /*return new Ember.RSVP.Promise(function(resolve) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var p = position.coords;
          resolve(self.store.find('service', {latitude: p.latitude, longitude: p.longitude}));
        });
      });*/
      return [];
    }
  });

  App.IndexRoute = Ember.Route.extend({
    model: function() {
      return {name : 'Phillip Burch'};
    }
  });

})();
