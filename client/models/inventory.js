var attr = Ember.attr, hasMany = Ember.hasMany;

App.Inventory = Ember.Model.extend({
  _id: attr('string'),
  name: attr('string'),
  image: attr('string'),
  price: attr('number'),
  quanity: attr('number')
});

App.Inventory.rootKey = 'inventory';
App.Inventory.url = '/api/inventory';
App.Inventory.adapter = Ember.RESTAdapter.create();
App.Inventory.primaryKey = '_id';
