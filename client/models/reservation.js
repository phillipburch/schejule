var attr = Ember.attr, hasMany = Ember.hasMany;

App.Reservation = Ember.Model.extend({
  eta: attr('date'),
  price: attr('number'),
  createdOn: attr('date'),
  carImage: attr('string'),
  claimed: attr('boolean'),
  license: attr('string'),
  longitude: attr('string'),
  latitude: attr('string'),
  address: attr('string'),
  loc: attr('raw')
});

App.Reservation.url = '/api/reservations'
App.Reservation.adapter = Ember.RESTAdapter.create();
App.Reservation.collectionKey = 'reservations';
