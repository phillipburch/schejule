var attr = Ember.attr, hasMany = Ember.hasMany;

App.User = Ember.Model.extend({
  name: attr('string'),
  phone: attr('string'),
  email: attr('string')
});

App.User.url = '/api/users';
App.User.adapter = Ember.RESTAdapter.create();