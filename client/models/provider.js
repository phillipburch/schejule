var attr = Ember.attr, hasMany = Ember.hasMany;

App.Provider = Ember.Model.extend({
  _id: attr('string'),
  name: attr('string'),
  image: attr('string'),
  price: attr('number'),
  loc: attr('raw'),
  inventorys: hasMany('App.Inventory', {embedded: true})
});

App.Provider.rootKey = 'provider';
App.Provider.url = '/api/providers'
App.Provider.adapter = Ember.RESTAdapter.create();
App.Provider.collectionKey = 'providers';