var attr = Ember.attr, hasMany = Ember.hasMany;

App.Match = Ember.Model.extend({
  _id: attr('string'),
  name: attr('string'),
  image: attr('string')
});

App.Match.rootKey = 'match';
App.Match.url = '/api/matches';
App.Match.adapter = Ember.RESTAdapter.create();
App.Match.primaryKey = '_id';
App.Match.collectionKey = 'matches';
