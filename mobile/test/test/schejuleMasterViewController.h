//
//  schejuleMasterViewController.h
//  test
//
//  Created by Phillip Burch on 4/9/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>

@class schejuleDetailViewController;

@interface schejuleMasterViewController : UITableViewController

@property (strong, nonatomic) schejuleDetailViewController *detailViewController;

@end
