//
//  schejuleOrderController.h
//  Schejule
//
//  Created by Phillip Burch on 3/25/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "schejuleUser.h"
#import "schejuleReservation.h"
#import "schejuleNewCustomerController.h"

@interface schejuleConfirmationController : UIViewController<schejuleNewCustomerControllerDelegate>
@property (strong, nonatomic) NSString* reservationId;
@end
