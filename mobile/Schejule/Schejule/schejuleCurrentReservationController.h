//
//  schejuleCurrentReservationController.h
//  Schejule
//
//  Created by Phillip Burch on 5/7/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "schejuleReservation.h"
#import "UIImageView+AFNetworking.h"
#import "schejuleReservationSource.h"

@interface schejuleCurrentReservationController : UIViewController
@property (strong, nonatomic) schejuleReservation * reservation;
@property (weak, nonatomic) IBOutlet UITextView *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *carImage;
- (IBAction)cancelAction:(id)sender;
@end
