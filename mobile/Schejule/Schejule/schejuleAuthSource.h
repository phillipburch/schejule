//
//  schejuleAuth.h
//  Schejule
//
//  Created by Phillip Burch on 4/14/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AFHTTPRequestOperationManager.h"
#import "schejuleConstants.h"

@class schejuleAuthSource;

@protocol schejuleAuthSourceDelegate <NSObject>
- (void)authenticated: (NSString * ) token;
- (void)authenticationError:(schejuleAuthSource *)schejuleServiceSource;
@end

@interface schejuleAuthSource : NSObject
@property NSUserDefaults *standardUserDefaults;
@property (nonatomic, weak) IBOutlet id <schejuleAuthSourceDelegate> delegate;
-(void) authenticate: (NSString *)userName password:(NSString *)password success:(void(^)(NSString * token)) successBlock error: (void(^)(NSError * error)) errorBlock;
-(void)signUp: (NSString *)email Phone:(NSString *)phone password: (NSString*)password success:(void(^)(NSString * token)) successBlock error: (void(^)(NSError * error)) errorBlock;
@end
