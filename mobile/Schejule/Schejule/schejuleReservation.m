//
//  schejuleReservation.m
//  Schejule
//
//  Created by Phillip Burch on 4/14/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejuleReservation.h"

@implementation schejuleReservation
@synthesize eta, price, _id, userName, carPicture, licensePlateNumber, address, latitude, longitude, createdOn;

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:eta forKey:@"eta"];
    [encoder encodeObject:price forKey:@"price"];
    [encoder encodeObject:_id forKey:@"_id"];
    [encoder encodeObject:userName forKey:@"userName"];
    [encoder encodeObject:carPicture forKey:@"carPicture"];
    [encoder encodeObject:licensePlateNumber forKey:@"licensePlateNumber"];
    [encoder encodeObject:address forKey:@"address"];
    [encoder encodeObject:[[NSNumber alloc] initWithDouble:latitude] forKey:@"latitude"];
    [encoder encodeObject:[[NSNumber alloc] initWithDouble:longitude] forKey:@"longitude"];
    [encoder encodeObject:createdOn forKey:@"createdOn"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        eta = [decoder decodeObjectForKey:@"eta"];
        price = [decoder decodeObjectForKey:@"price"];
        _id = [decoder decodeObjectForKey:@"_id"];
        userName = [decoder decodeObjectForKey:@"userName"];
        carPicture = [decoder decodeObjectForKey:@"carPicture"];
        licensePlateNumber = [decoder decodeObjectForKey:@"licensePlateNumber"];
        address = [decoder decodeObjectForKey:@"address"];
        latitude = [[decoder decodeObjectForKey:@"latitude"] doubleValue];
        longitude = [[decoder decodeObjectForKey:@"longitude"] doubleValue];
        createdOn = [decoder decodeObjectForKey:@"createdOn"];
    }
    return self;
}

@end
