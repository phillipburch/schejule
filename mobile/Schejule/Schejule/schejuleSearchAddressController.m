//
//  schejuleSearchAddressController.m
//  Schejule
//
//  Created by Phillip Burch on 4/15/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejuleSearchAddressController.h"
#import "schejuleAddressTableCell.h"

@interface schejuleSearchAddressController ()

@end

@implementation schejuleSearchAddressController
@synthesize tableView;

- (void)viewDidLoad
{
    // Do any additional setup after loading the view.
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.results = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.results count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 71;
}

- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath {
    
    NSLog(@"IndexPath: %ld", (long)indexPath.row);
    
    [self.delegate addressChosen: [self.results objectAtIndex:indexPath.row]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    schejuleAddressTableCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"addressCell"];
    CLPlacemark *placemark = [self.results objectAtIndex:indexPath.row];
    NSArray *lines = placemark.addressDictionary[ @"FormattedAddressLines"];
    NSString *addressString = [lines componentsJoinedByString:@" "];

    cell.titleLabel.text = addressString;
    
    
    return cell;
}


// Converts an address to lat/long. Also normalizes addresses.
-(void)setMapAtAddress: (NSString *)address {
    if (!self.geoCoder) {
        self.geoCoder = [[CLGeocoder alloc] init];
    }
    
    [self.geoCoder geocodeAddressString:address completionHandler:^(NSArray * placemarks, NSError *error) {
        [self.results removeAllObjects];
        [self.results addObjectsFromArray:placemarks];
        
        [self.searchDisplayController.searchResultsTableView reloadData];

    }];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    NSString* text = self.searchDisplayController.searchBar.text;
    [self setMapAtAddress:text];
    return NO;
}

- (IBAction)cancelButton:(id)sender {
    [self.delegate addressChosen: nil];
}

@end
