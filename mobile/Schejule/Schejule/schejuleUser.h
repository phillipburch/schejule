//
//  schejuleUser.h
//  Schejule
//
//  Created by Phillip Burch on 4/14/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface schejuleUser : NSObject
@property NSString* name;
@property NSString* password;
@property NSString* email;
@property NSString* _id;
@end
