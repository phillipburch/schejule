//
//  schejuleReservationSource.m
//  Schejule
//
//  Created by Phillip Burch on 4/14/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejuleReservationSource.h"

@interface schejuleReservationSource (){
    NSUserDefaults * standardUserDefaults;
}
@end

@implementation schejuleReservationSource
@synthesize totalItems, searchQuery, responseData, Results, page, IsLoading;

- (id) init {
    if(self = [super init]) {
        standardUserDefaults = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

-(id)initWithQuery:(NSString *)query
{
    if (self = [super init]) {
        self.searchQuery = query;
        [self find:query];
    }
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
    Results = [[NSMutableArray alloc] init];
    return self;
    
}

- (AFHTTPRequestOperationManager *) getManager{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:BaseUrl]];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    return manager;
}

-(void) createReservation: (NSMutableDictionary *)reservation withImageData:(NSData *)imageData success:(void(^)(schejuleReservation * reservation))successBlock error: (void(^)(NSError * error)) errorBlock {
    
    NSString *token = [standardUserDefaults stringForKey:@"AuthToken"];
    NSMutableDictionary* parameters = [reservation mutableCopy];

    [parameters setObject:token forKey:@"token"];

    AFHTTPRequestOperationManager *manager = [self getManager];
    
    AFHTTPRequestOperation *op = [manager POST:@"/api/reservations" parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
  
        [formData appendPartWithFileData:imageData name:@"carImage" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        schejuleReservation *r = [self convertDictionary:[responseObject objectForKey:@"reservation"]];
        
        if (successBlock && r._id != nil) {
            successBlock(r);
        } else {
            if(errorBlock) {
                NSDictionary *userInfo = @{
                                           NSLocalizedDescriptionKey: NSLocalizedString(@"Oops", nil),
                                           NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Something went wrong.", nil)
                                           };
                errorBlock([NSError errorWithDomain:@"login" code:101 userInfo:userInfo]);
            }
        }
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(errorBlock) {
            NSDictionary *userInfo = @{
                                       NSLocalizedDescriptionKey: NSLocalizedString(@"Oops", nil),
                                       NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Something went wrong.", nil)
                                       };
            errorBlock([NSError errorWithDomain:@"Reservation" code:101 userInfo:userInfo]);
        }
    }];
    [op start];
}

-(void) createReservation: (NSMutableDictionary *)reservation withLicensePlate:(NSString *)plateNumber success:(void(^)(schejuleReservation * reservation))successBlock error: (void(^)(NSError * error)) errorBlock {
    
    NSString *token = [standardUserDefaults stringForKey:@"AuthToken"];
    NSMutableDictionary* parameters = [reservation mutableCopy];

    [parameters setObject:token forKey:@"token"];
    [parameters setObject:plateNumber forKey:@"license"];
    
    AFHTTPRequestOperationManager *manager = [self getManager];
    AFHTTPRequestOperation *op = [manager POST:@"/api/reservations" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        schejuleReservation *r = [self convertDictionary:[responseObject objectForKey:@"reservation"]];
        
        if (successBlock && r._id != nil) {
            successBlock(r);
        } else {
            if(errorBlock) {
                NSDictionary *userInfo = @{
                                           NSLocalizedDescriptionKey: NSLocalizedString(@"Oops", nil),
                                           NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Something went wrong.", nil)
                                           };
                errorBlock([NSError errorWithDomain:@"login" code:101 userInfo:userInfo]);
            }
        }
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(errorBlock) {
            NSDictionary *userInfo = @{
                                       NSLocalizedDescriptionKey: NSLocalizedString(@"Oops", nil),
                                       NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Something went wrong.", nil)
                                       };
            errorBlock([NSError errorWithDomain:@"Reservation" code:101 userInfo:userInfo]);
        }
    }];
    
    [op start];
}


-(void) cancelReservation: (NSString *)reservationId success:(void(^)())successBlock error:(void(^)())errorBlock
{
    NSString *token = [standardUserDefaults stringForKey:@"AuthToken"];
    NSMutableDictionary* parameters = [@{@"token": token} mutableCopy];
    
    AFHTTPRequestOperationManager *manager = [self getManager];
    
    NSString * url = [NSString stringWithFormat:@"/api/reservations/%@/cancel", reservationId];
    
    AFHTTPRequestOperation *op = [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(successBlock){
            successBlock();
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(errorBlock){
            errorBlock();
        }
    }];
    
    [op start];
}

-(void) createCustomer: (NSString *)paymentToken success:(void(^)(NSString * customer))successBlock error: (void(^)(NSError * error)) errorBlock {

    NSString *token = [standardUserDefaults stringForKey:@"AuthToken"];
    NSMutableDictionary* parameters = [@{@"token": token, @"paymentToken": paymentToken} mutableCopy];
    
    AFHTTPRequestOperationManager *manager = [self getManager];
    
    NSString * url = [NSString stringWithFormat:@"/api/customers"];
    AFHTTPRequestOperation *op = [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString * customerId = [responseObject objectForKey:@"id"];
        [standardUserDefaults setValue:customerId forKey:@"customerId"];
        [standardUserDefaults synchronize];
        successBlock(customerId);
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
    }];
    
    [op start];
}


-(void) find:(NSString*) query {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    NSString *token = [standardUserDefaults stringForKey:@"AuthToken"];
    
    NSDictionary *parameters = @{@"access_token": token};
    NSString *url = [NSString stringWithFormat:@"%1$@/api/group", BaseUrl];
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self requestFinished:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

-(schejuleReservation *) convertDictionary:(NSDictionary *)dictionary {
    NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    schejuleReservation *reservation = [[schejuleReservation alloc] init];
    
    [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"];
	[rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];

    reservation.eta = [dictionary objectForKey:@"eta"];
    reservation.price = [dictionary objectForKey:@"price"];
    reservation._id = [dictionary objectForKey:@"_id"];
    reservation.carPicture = [dictionary objectForKey:@"carImage"];
    reservation.licensePlateNumber = [dictionary objectForKey:@"license"];
    reservation.address = [dictionary objectForKey:@"address"];
    reservation.latitude = [[dictionary objectForKey:@"latitude"] doubleValue];
    reservation.longitude = [[dictionary objectForKey:@"longitude"] doubleValue];
    reservation.createdOn = [rfc3339DateFormatter dateFromString:[dictionary objectForKey:@"createdOn"] ];
    
    if([dictionary objectForKey:@"user"] != (id)[NSNull null])
        reservation.userName = [[dictionary objectForKey:@"user"] objectForKey:@"email"];
    
    return reservation;
}

-(NSMutableArray *) requestFinished:(NSDictionary*)items{
    NSArray *reservations = [items objectForKey:@"reservations"];
    NSMutableArray* myResults = [[NSMutableArray alloc] init];
    
    for (NSDictionary* c in reservations) {
        [myResults addObject:[self convertDictionary:c]];
    }
    
    return myResults;
}


@end
