//
//  schejulePaymentSource.h
//  Schejule
//
//  Created by phillp burch on 4/28/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

@class schejulePaymentSource;

@protocol schejulePaymentSourceDelegate <NSObject>
- (void)dataLoaded:(schejulePaymentSource *)schejulePaymentSource;
@end


@interface schejulePaymentSource : NSObject
@property (strong) NSArray* totalItems;
@property (copy) NSString * searchQuery;
@property (strong) NSMutableData* responseData;
@property NSMutableArray* Results;
@property int page;
@property BOOL IsLoading;
@property BOOL CanLoadMore;
-(void) chargeCustomer:(NSString *)customerId forReservation: (NSString *) reservation success:(void(^)(NSString * reservationId))successBlock error: (void(^)(NSError * error)) errorBlock;
@property NSUserDefaults *standardUserDefaults;
@property (nonatomic, weak) IBOutlet id <schejulePaymentSourceDelegate> delegate;
@end
