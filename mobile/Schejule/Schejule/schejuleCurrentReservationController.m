//
//  schejuleCurrentReservationController.m
//  Schejule
//
//  Created by Phillip Burch on 5/7/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejuleCurrentReservationController.h"
#import "DateTools.h"

@interface schejuleCurrentReservationController ()
{
    NSUserDefaults *_standardUserDefaults;
    schejuleReservationSource *_datasource;
}
@end

@implementation schejuleCurrentReservationController
@synthesize reservation, addressLabel, timeLabel, carImage;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _datasource = [[schejuleReservationSource alloc]init];
    _standardUserDefaults = [NSUserDefaults standardUserDefaults];
}

-(void) viewWillAppear:(BOOL)animated{
    NSData * data = [_standardUserDefaults objectForKey:@"currentReservation"];
    reservation = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    addressLabel.text = reservation.address;
    timeLabel.text = reservation.createdOn.timeAgoSinceNow;
    //    licensePlate.text = reservation.licensePlateNumber;

    [carImage setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", BaseUrl, reservation.carPicture]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelAction:(id)sender {
    [_datasource cancelReservation:reservation._id success:^{
        [_standardUserDefaults removeObjectForKey:@"currentReservation"];
        [_standardUserDefaults synchronize];
        [self dismissViewControllerAnimated:YES completion:nil];
    } error:nil];
}
@end
