//
//  schejuleReservationSource.h
//  Schejule
//
//  Created by Phillip Burch on 4/14/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

#import "schejuleReservation.h"

@class schejuleReservationSource;

@protocol schejuleReservationSourceDelegate <NSObject>
- (void)dataLoaded:(schejuleReservationSource *)schejuleReservationSource;
@end

@interface schejuleReservationSource : NSObject
@property (strong) NSArray* totalItems;
@property (copy) NSString * searchQuery;
@property (strong) NSMutableData* responseData;
@property NSMutableArray* Results;
@property int page;
@property BOOL IsLoading;
@property BOOL CanLoadMore;
-(void) createReservation: (NSDictionary *)reservation withImageData:(NSData *)imageData success:(void(^)(schejuleReservation * reservation))successBlock error: (void(^)(NSError * error)) errorBlock;

-(void) createReservation: (NSDictionary *)reservation withLicensePlate:(NSString *)plateNumber success:(void(^)(schejuleReservation * reservation))successBlock error: (void(^)(NSError * error)) errorBlock;

- (id)initWithQuery:(NSString*)query;

-(void) createCustomer: (NSString *)paymentToken success:(void(^)(NSString * customer))successBlock error: (void(^)(NSError * error)) errorBlock;

- (void)find:(NSString *)query;

-(void) cancelReservation: (NSString *)reservationId success:(void(^)())successBlock error:(void(^)())errorBlock;

@property (nonatomic, weak) IBOutlet id <schejuleReservationSourceDelegate> delegate;
@end
