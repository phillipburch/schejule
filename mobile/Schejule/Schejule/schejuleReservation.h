//
//  schejuleReservation.h
//  Schejule
//
//  Created by Phillip Burch on 4/14/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface schejuleReservation : NSObject
@property NSNumber *eta;
@property NSNumber *price;
@property NSString *_id;
@property NSString *userName;
@property NSString *carPicture;
@property NSString *licensePlateNumber;
@property NSString *address;
@property double longitude;
@property double latitude;
@property NSDate *createdOn;
@end
