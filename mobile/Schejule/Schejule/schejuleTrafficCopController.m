//
//  schejuleTrafficCopController.m
//  Schejule
//
//  Created by Phillip Burch on 4/17/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejuleTrafficCopController.h"

@interface schejuleTrafficCopController ()

@end

@implementation schejuleTrafficCopController
@synthesize standardUserDefaults;

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([standardUserDefaults boolForKey:@"IsAuthenticated"] == YES) {
            // Do any additional setup after loading the view.
        [self performSegueWithIdentifier:@"toApp" sender:self];

    } else {
            // Do any additional setup after loading the view.
        [self performSegueWithIdentifier:@"toLogin" sender:self];

    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
