//
//  schejulePaymentController.h
//  Schejule
//
//  Created by Phillip Burch on 3/25/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "STPView.h"
#import "schejuleUser.h"
#import "schejuleReservation.h"
#import "schejuleReservationSource.h"

@class schejuleNewCustomerController;

@protocol schejuleNewCustomerControllerDelegate <NSObject>
- (void)dataLoaded:(NSString *)customerId;
@end

@interface schejuleNewCustomerController :  UIViewController <STPViewDelegate>
{
    schejuleReservationSource *_datasource;
}
@property (strong, nonatomic) NSString* reservationId;
@property STPView* checkoutView;
@property (nonatomic, strong) IBOutlet id <schejuleNewCustomerControllerDelegate> delegate;
- (IBAction)save:(id)sender;
- (IBAction)cancel:(id)sender;

@end
