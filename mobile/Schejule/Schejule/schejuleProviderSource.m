//
//  schejuleProviderSource.m
//  Schejule
//
//  Created by Phillip Burch on 4/29/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejuleProviderSource.h"

@implementation schejuleProviderSource
@synthesize totalItems, searchQuery, responseData, Results, page, IsLoading, standardUserDefaults;


-(void) findAtLong:(double)longitude andLat:(double)latitude success:(void(^)(NSMutableArray * providers)) successBlock error: (void(^)(NSError * error)) errorBlock{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
//    NSString *token = [standardUserDefaults stringForKey:@"AuthToken"];
    
    NSDictionary *parameters = @{@"longitude": [NSNumber numberWithDouble:longitude], @"latitude": [NSNumber numberWithFloat:latitude]};
    
    NSString *url = [NSString stringWithFormat:@"%1$@/api/providers", BaseUrl];
    
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if(successBlock) {
            successBlock([self requestFinished:responseObject]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

-(NSMutableArray *) requestFinished:(NSDictionary*)items{
    NSArray *providers = [items objectForKey:@"providers"];
    NSMutableArray* myResults = [[NSMutableArray alloc] init];
    
    for (NSDictionary* c in providers) {
        schejuleProvider *provider = [[schejuleProvider alloc] init];
        
        provider.name = [c objectForKey:@"name"];
        provider._id = [c objectForKey:@"_id"];
        provider.longitude = [c objectForKey:@"longitude"];
        provider.latitude = [c objectForKey:@"latitude"];
        if([c objectForKey:@"user"] != (id)[NSNull null])
            provider.user = [[c objectForKey:@"user"] objectForKey:@"name"];
        [myResults addObject:provider];
    }
    return myResults;
}

@end
