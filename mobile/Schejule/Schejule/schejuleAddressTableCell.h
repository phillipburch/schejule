//
//  schejuleAddressTableCell.h
//  Schejule
//
//  Created by Phillip Burch on 4/15/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface schejuleAddressTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
