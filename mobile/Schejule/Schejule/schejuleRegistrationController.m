//
//  schejuleRegistrationController.m
//  Schejule
//
//  Created by Phillip Burch on 4/18/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejuleRegistrationController.h"

@interface schejuleRegistrationController ()
{
    schejuleAuthSource *_datasource;
}
@end

@implementation schejuleRegistrationController
@synthesize emailTextField, phoneTextField, passwordTextField, standardUserDefaults;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    [self addPadddingTo:phoneTextField];
    [self addPadddingTo:passwordTextField];

    passwordTextField.delegate = self;
    phoneTextField.delegate = self;
    
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    _datasource = [[schejuleAuthSource alloc]init];
}

-(void)addPadddingTo:(UITextField*)textField{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

- (void) markInvalid:(UITextField*)textField{
    textField.layer.borderColor=[[UIColor redColor]CGColor];
    textField.layer.borderWidth= 4.0f;
}

- (void) markValid:(UITextField*)textField{
    textField.layer.borderColor=[[UIColor redColor]CGColor];
    textField.layer.borderWidth= 0.0f;
}

- (void) alertOf:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error.localizedDescription
                                                    message:error.localizedRecoverySuggestion
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    [alert show];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField != phoneTextField) return YES;
    
    int length = [self getLength:textField.text];
    //NSLog(@"Length  =  %d ",length);
    
    if(length == 10)
    {
        if(range.length == 0)
            return NO;
    }
    
    if(length == 3)
    {
        NSString *num = [self formatNumber:textField.text];
        textField.text = [NSString stringWithFormat:@"(%@) ",num];
        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
    }
    else if(length == 6)
    {
        NSString *num = [self formatNumber:textField.text];
        //NSLog(@"%@",[num  substringToIndex:3]);
        //NSLog(@"%@",[num substringFromIndex:3]);
        textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
    }
    
    return YES;
}

-(NSString*)formatNumber:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = [mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        NSLog(@"%@", mobileNumber);
        
    }
    
    
    return mobileNumber;
}


-(int)getLength:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = [mobileNumber length];
    
    return length;
}


- (void)authenticated:(NSString *)token{
    [standardUserDefaults setBool:YES forKey:@"IsAuthenticated" ];
    [standardUserDefaults setValue:token forKey:@"AuthToken" ];
    [standardUserDefaults synchronize];
    [self performSegueWithIdentifier:@"toApp" sender:self];
}

-(void)dismissKeyboard {
    [phoneTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
}

-(void) authenticationError:(schejuleAuthSource *)schejuleServiceSource{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField.text.length > 0) {
        [self signupAction:textField];
        return YES;
    }
    return NO;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (IBAction)signupAction:(id)sender {
    [_datasource signUp:emailTextField.text Phone:phoneTextField.text password:passwordTextField.text success:^(NSString* token){
        [standardUserDefaults setBool:YES forKey:@"IsAuthenticated" ];
        [standardUserDefaults setValue:token forKey:@"AuthToken" ];
        [standardUserDefaults synchronize];
        [self.navigationController dismissViewControllerAnimated:NO completion:nil];
        [self performSegueWithIdentifier:@"toApp" sender:self];
    } error:^(NSError * error) {
        [self alertOf:error];
    }];
}
@end
