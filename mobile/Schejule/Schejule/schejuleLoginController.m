//
//  schejuleLoginController.m
//  Schejule
//
//  Created by Phillip Burch on 4/14/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejuleLoginController.h"

@interface schejuleLoginController (){
    schejuleAuthSource *_datasource;
}

@end

@implementation schejuleLoginController
@synthesize emailTextField, passwordTextField, standardUserDefaults;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [self addPadddingTo:emailTextField];
    [self addPadddingTo:passwordTextField];
    
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    _datasource = [[schejuleAuthSource alloc]init];
    _datasource.delegate = self;
    
    passwordTextField.delegate = self;
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [emailTextField becomeFirstResponder];
}

-(void)addPadddingTo:(UITextField*)textField{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

- (void) markInvalid:(UITextField*)textField{
    textField.layer.borderColor=[[UIColor redColor]CGColor];
    textField.layer.borderWidth= 4.0f;
}

- (void) markValid:(UITextField*)textField{
    textField.layer.borderColor=[[UIColor redColor]CGColor];
    textField.layer.borderWidth= 0.0f;
}

- (void) alertOf:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error.localizedDescription
                                                    message:error.localizedRecoverySuggestion
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    [alert show];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField.text.length > 0) {
        [self loginAction:textField];
        return YES;
    }
    return NO;
}

- (void)authenticated:(NSString *)token{

}

-(void)dismissKeyboard {
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
}

-(void) authenticationError:(schejuleAuthSource *)schejuleServiceSource{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginAction:(id)sender {
    [_datasource authenticate:emailTextField.text password:passwordTextField.text success:^(NSString * token){
        [standardUserDefaults setBool:YES forKey:@"IsAuthenticated" ];
        [standardUserDefaults setValue:token forKey:@"AuthToken" ];
        [standardUserDefaults synchronize];
        [self.navigationController dismissViewControllerAnimated:NO completion:nil];
        [self performSegueWithIdentifier:@"toApp" sender:self];
    } error:^(NSError * error) {
        [self alertOf:error];
    }];
}
@end
