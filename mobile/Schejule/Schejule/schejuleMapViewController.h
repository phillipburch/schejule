//
//  schejuleMasterViewController.h
//  Schejule
//
//  Created by Phillip Burch on 3/24/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "schejuleUser.h"
#import "schejuleProviderSource.h"
#import "schejuleSearchAddressController.h"
#import "schejuleCarDetailsController.h"

@interface schejuleMapViewController : UIViewController<MKMapViewDelegate, schejuleAddressControllerDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) schejuleSearchAddressController *addressSearchController;

@property (strong, nonatomic) CLLocationManager *locationManager;
-(void)toMyLocationAction:(id)sender;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic)   CLGeocoder * geoCoder;
@property (weak, nonatomic) IBOutlet UIButton *addressButton;
@property (strong, nonatomic) NSString* subtitle;
@property (strong, nonatomic) NSDate* dateAdded;
@property (weak, nonatomic) IBOutlet UIImageView *locationImageView;
@property (weak, nonatomic) IBOutlet UIButton *requestCarwashButton;
@property (strong, nonatomic) NSMutableDictionary *coordinates;
@end
