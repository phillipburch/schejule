//
//  schejulePaymentSource.m
//  Schejule
//
//  Created by phillp burch on 4/28/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejulePaymentSource.h"

@implementation schejulePaymentSource

@synthesize totalItems, searchQuery, responseData, Results, page, IsLoading, standardUserDefaults;


-(void) chargeCustomer:(NSString *)customerId forReservation: (NSString * )reservation success:(void(^)(NSString * reservationId))successBlock error: (void(^)(NSError * error)) errorBlock {
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [standardUserDefaults stringForKey:@"AuthToken"];
    
    NSDictionary *parameters = @{@"token": token, @"customerId": customerId};
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:BaseUrl]];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString * url = [NSString stringWithFormat:@"/api/reservations/%@/charge", reservation];
    
    AFHTTPRequestOperation *op = [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(successBlock) {
            successBlock(@"");
        }
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(errorBlock) {
            errorBlock(error);
        }
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
    }];
    
    [op start];
}
@end
