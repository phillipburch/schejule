//
//  schejuleCarDetailsController.m
//  Schejule
//
//  Created by Phillip Burch on 4/16/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejuleCarDetailsController.h"


@interface schejuleCarDetailsController (){
schejuleReservationSource *_datasource;
    schejulePaymentSource *paymentSource;
    NSString * _reservationId;
    NSUserDefaults *standardUserDefaults;
}
@end

@implementation schejuleCarDetailsController
@synthesize licensePlateText;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    licensePlateText.delegate = self;
    _datasource = [[schejuleReservationSource alloc] init];
    paymentSource = [[schejulePaymentSource alloc]init];
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //put code for store image
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    NSData *imageData = UIImageJPEGRepresentation(chosenImage, 0.5);

    [_datasource createReservation:self.reservationData withImageData:imageData success:^(schejuleReservation * reservation){
        _reservationId = reservation._id;

        [picker dismissViewControllerAnimated:YES completion:^{
            [self savedReservation:reservation];        }];
    } error:^(NSError * error) {
        [self alertOf:error];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) dataLoaded:(NSString * )customerId{
    [self dismissViewControllerAnimated:NO completion:nil];
    [self performSegueWithIdentifier:@"toCurrent" sender:self];

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"toPayment"])
    {
        schejuleNewCustomerController *ccController = [segue destinationViewController];
        ccController.delegate = self;
        ccController.reservationId = _reservationId;
    }
}

-(void) savedReservation:(schejuleReservation *) reservation{
    NSData *rencoded = [NSKeyedArchiver archivedDataWithRootObject:reservation];
    
    [standardUserDefaults setObject:rencoded forKey:@"currentReservation"];
    [standardUserDefaults synchronize];
    
    NSString * customerId = [standardUserDefaults objectForKey:@"customerId"];
    if(customerId == nil) {
        [self performSegueWithIdentifier:@"toPayment" sender:self];
    } else{
        [paymentSource chargeCustomer:customerId forReservation:reservation._id success:^(NSString * reservationId){
            [self performSegueWithIdentifier:@"toCurrent" sender:self];
        } error:nil];
        
    }
}

- (IBAction)takePicture:(id)sender {
    self.picker = [[UIImagePickerController alloc] init];
    [self.picker setDelegate:self];
    self.picker.allowsEditing = YES;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"Take photo", @"Choose Existing", nil];
        [actionSheet showInView:self.view];
    } else {
        self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.picker
                           animated:YES completion:nil];
    }
}

- (void) alertOf:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error.localizedDescription
                                                    message:error.localizedRecoverySuggestion
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    [alert show];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_datasource createReservation:self.reservationData withLicensePlate:textField.text success:^(schejuleReservation * reservation) {
        _reservationId = reservation._id;

        [self savedReservation:reservation];

    } error:^(NSError * error) {
        [self alertOf:error];
    }];
    return YES;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else if (buttonIndex == 1) {
        self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    [self presentViewController:self.picker
                       animated:YES completion:nil];

}

@end
