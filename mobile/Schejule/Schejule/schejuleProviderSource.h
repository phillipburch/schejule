//
//  schejuleProviderSource.h
//  Schejule
//
//  Created by Phillip Burch on 4/29/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"
#import "schejuleProvider.h"

@interface schejuleProviderSource : NSObject
@property (strong) NSArray* totalItems;
@property (copy) NSString * searchQuery;
@property (strong) NSMutableData* responseData;
@property NSMutableArray* Results;
@property int page;
@property BOOL IsLoading;
@property BOOL CanLoadMore;
-(void) findAtLong:(double)longitude andLat:(double)latitude success:(void(^)(NSMutableArray * providers)) successBlock error: (void(^)(NSError * error)) errorBlock ;
@property NSUserDefaults *standardUserDefaults;
@end
