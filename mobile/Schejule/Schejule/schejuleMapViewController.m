//
//  schejuleMasterViewController.m
//  Schejule
//
//  Created by Phillip Burch on 3/24/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejuleMapViewController.h"


@interface schejuleMapViewController () {
    NSMutableArray *_objects;
    NSMutableArray *points;
    NSUserDefaults *_standardUserDefaults;
    schejuleProviderSource *_dataSource;
    Boolean locationManuallySet;
    CLLocationManager *_locationManager;
    NSString *addressString;
    Boolean _MapSet;
}
@end

@implementation schejuleMapViewController
@synthesize geoCoder, subtitle, dateAdded, addressButton, locationImageView;
- (void)awakeFromNib
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }

    _dataSource = [[schejuleProviderSource alloc] init];
    _MapSet = NO;
    locationManuallySet = NO;

    points = [[NSMutableArray alloc] init];
    [super awakeFromNib];
}

- (void) providersLoaded: (NSMutableArray *) response{
    for(schejuleProvider* service in response) {
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//        point.coordinate = service.latitude;
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([service.latitude doubleValue], [service.longitude doubleValue]);
        point.coordinate = coordinate;
        point.title = service.name;
        point.subtitle = service.user;
        [points addObject:point];
        [self.mapView addAnnotation:point];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mapView.mapType = MKMapTypeStandard;
    self.mapView.rotateEnabled = NO;
    self.mapView.delegate = self;
    [self.requestCarwashButton setEnabled:NO];
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toMyLocationAction:)];
    
    [singleTap setNumberOfTapsRequired:1];
    [locationImageView addGestureRecognizer:singleTap];
}

-(void)toMyLocationAction:(id)sender{
    locationManuallySet = NO;
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    [_locationManager startUpdatingLocation];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    double latitude = [_standardUserDefaults doubleForKey:@"latitude"];
    double longitude = [_standardUserDefaults doubleForKey:@"longitude"];
    // Just converting to nsnumber to check for nil... odd?
    if(latitude != 0 && longitude != 0) {
        [_dataSource findAtLong:longitude andLat:latitude success:^(NSMutableArray * response) {
            [self providersLoaded:response];
        } error:nil];
    }
    
    if (!self.locationManager) {
        self.locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        [_locationManager startUpdatingLocation];
    }
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if([_standardUserDefaults objectForKey:@"currentReservation"] != nil) {
        [self performSegueWithIdentifier:@"toCurrent" sender:nil];
        return;
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%@", error);
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    if(locationManuallySet) return;
    CLLocation *location = [locations lastObject];
//    NSLog(@"%@", locations);

     if (location.horizontalAccuracy <= manager.desiredAccuracy) {
         [self setCurrentCity: location recenterMap:YES];
         [self.locationManager startMonitoringSignificantLocationChanges];
         [self.locationManager stopUpdatingLocation];
         [_dataSource findAtLong:location.coordinate.longitude andLat:location.coordinate.latitude success:^(NSMutableArray * response) {
             [self providersLoaded:response];
         } error:nil];
     }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    if(_MapSet == NO) return;
   CLLocationCoordinate2D center = [mapView centerCoordinate];
    [self setCurrentCity: [[CLLocation alloc] initWithLatitude:center.latitude longitude:center.longitude] recenterMap:NO];
}

// Converts an address to lat/long. Also normalizes addresses.

-(void) updateAddressLabel: (CLPlacemark *) placemark{
    self.subtitle = [NSString stringWithFormat:@"%@ %@",
                     placemark.subThoroughfare, placemark.thoroughfare
                     ];
    
    NSArray *lines = placemark.addressDictionary[ @"FormattedAddressLines"];
    NSString *address = [lines componentsJoinedByString:@" "];
    addressString = address;
    [self.requestCarwashButton setEnabled:YES];
    [self.addressButton setTitle:self.subtitle forState:UIControlStateNormal];
}

-(void) setMapAtPlacemark: (CLPlacemark *) placemark{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(placemark.location.coordinate, 800, 800);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:NO];
}

-(void)setCurrentCity: (CLLocation *)location recenterMap: (Boolean) recenter {

    if (!self.geoCoder) {
        self.geoCoder = [[CLGeocoder alloc] init];
    }
    NSString *latitude = @(location.coordinate.latitude).stringValue;
                           
    NSString *longitude = @(location.coordinate.longitude).stringValue;
                           
    self.coordinates = [@{@"longitude": longitude, @"latitude": latitude} mutableCopy];

    if(recenter == YES) {
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location.coordinate, 800, 800);
        [self.mapView setRegion:[self.mapView regionThatFits:region] animated:NO];
        _MapSet = YES;
    }

    [self.geoCoder reverseGeocodeLocation: location completionHandler:
     ^(NSArray *placemarks, NSError *error) {
         if([placemarks count] > 0){
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSLog(@"%@", placemark);
             [self updateAddressLabel:placemark];
         }
     }];
}
- (void) addressChosen:(CLPlacemark *)placemark {
    if(placemark != nil){
        locationManuallySet = YES;
        [self setMapAtPlacemark:placemark];
        [self updateAddressLabel:placemark];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"toSearchAddress"])
    {
        // Get reference to the destination view controller
        self.addressSearchController = [[[segue destinationViewController] viewControllers] objectAtIndex:0];
        self.addressSearchController.delegate = self;
    } else if([[segue identifier] isEqualToString:@"toCarDetails"])
    {
        schejuleCarDetailsController *detailsController = [segue destinationViewController];
        
        detailsController.reservationData = self.coordinates;
        [detailsController.reservationData setObject:addressString forKey:@"address"];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
