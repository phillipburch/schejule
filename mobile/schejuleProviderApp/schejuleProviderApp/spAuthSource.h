//
//  spAuth.h
//  sp
//
//  Created by Phillip Burch on 4/14/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AFHTTPRequestOperationManager.h"
#import "spConstants.h"

@class spAuthSource;

@protocol spAuthSourceDelegate <NSObject>
- (void)authenticated: (NSString * ) token;
- (void)authenticationError:(spAuthSource *)spServiceSource;
@end

@interface spAuthSource : NSObject
@property NSUserDefaults *standardUserDefaults;
@property (nonatomic, weak) IBOutlet id <spAuthSourceDelegate> delegate;

-(void) authenticate: (NSString *)userName password:(NSString *)password success:(void(^)(NSString * token)) successBlock error: (void(^)(NSError * error)) errorBlock;
-(void)signUp: (NSString *)email Phone:(NSString *)phone password: (NSString*)password success:(void(^)(NSString * token)) successBlock error: (void(^)(NSError * error)) errorBlock;
@end
