//
//  spCurrentWashController.m
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/5/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "spCurrentWashController.h"

@interface spCurrentWashController () {
    NSUserDefaults *_standardUserDefaults;
}

@end

@implementation spCurrentWashController
@synthesize addressLabel, currentImage, reservation, licensePlate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSData * data = [_standardUserDefaults objectForKey:@"claimedReservation"];
    reservation = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    addressLabel.text = reservation.address;
//    licensePlate.text = reservation.licensePlateNumber;
    
    [currentImage setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", BaseUrl, reservation.carPicture]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"toCarDetails"]){
        spCarDetailsController *detailController = [segue destinationViewController];
        
        detailController.reservation = self.reservation;
    }}

- (IBAction)directionsAction:(id)sender {
    // Create an MKMapItem to pass to the Maps app
    CLLocationCoordinate2D coordinate =
    CLLocationCoordinate2DMake(reservation.latitude, reservation.longitude);
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                   addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:@"Carwash location"];
    
    // Set the directions mode to "Walking"
    // Can use MKLaunchOptionsDirectionsModeDriving instead
    NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
    // Get the "Current User Location" MKMapItem
    MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
    // Pass the current location and destination map items to the Maps app
    // Set the direction mode in the launchOptions dictionary
    [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem]
                   launchOptions:launchOptions];
}
@end
