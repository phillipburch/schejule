//
//  schejuleCarDetailsController.m
//  Schejule
//
//  Created by Phillip Burch on 4/16/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejuleCarDetailsController.h"
#import "schejuleConfirmationController.h"

@interface schejuleCarDetailsController (){
schejuleReservationSource *_datasource;
    NSString * _reservationId;
}
@end

@implementation schejuleCarDetailsController
@synthesize licensePlateText;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    licensePlateText.delegate = self;
    _datasource = [[schejuleReservationSource alloc] init];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //put code for store image
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    NSData *imageData = UIImageJPEGRepresentation(chosenImage, 0.5);

    [_datasource createReservation:self.reservationData withImageData:imageData success:^(NSString * reservationId){
        _reservationId = reservationId;
        [picker dismissViewControllerAnimated:YES completion:^{
            [self performSegueWithIdentifier:@"toPay" sender:self];
        }];
    } error:nil];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"toPay"])
    {
        schejuleConfirmationController *orderController = [segue destinationViewController];
        
        orderController.reservationId = _reservationId;
    }
}


- (IBAction)takePicture:(id)sender {
    self.picker = [[UIImagePickerController alloc] init];
    [self.picker setDelegate:self];
    self.picker.allowsEditing = YES;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"Take photo", @"Choose Existing", nil];
        [actionSheet showInView:self.view];
    } else {
        self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.picker
                           animated:YES completion:nil];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_datasource createReservation:self.reservationData withLicensePlate:textField.text success:^(NSString * reservationId) {
        [self performSegueWithIdentifier:@"toPay" sender:self];
    } error:nil];
    return YES;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else if (buttonIndex == 1) {
        self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    [self presentViewController:self.picker
                       animated:YES completion:nil];

}

@end
