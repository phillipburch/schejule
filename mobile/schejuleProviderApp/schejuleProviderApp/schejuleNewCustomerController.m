//
//  schejulePaymentController.m
//  Schejule
//
//  Created by Phillip Burch on 3/25/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejuleNewCustomerController.h"
#import "MBProgressHUD.h"
#import "STPView.h"
#define STRIPE_PUBLISHABLE_KEY @"pk_d59Tvl13kmiX0YOXPItXsVHYWs3Wf"




@implementation schejuleNewCustomerController
@synthesize reservationId, delegate;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Add Card";
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // Setup save button
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(save:)];
    saveButton.enabled = NO;
    self.navigationItem.rightBarButtonItem = saveButton;
    
    _datasource = [[schejuleReservationSource alloc] init];

    // Setup checkout
    self.checkoutView = [[STPView alloc] initWithFrame:CGRectMake(15,20,290,55) andKey:STRIPE_PUBLISHABLE_KEY];
    self.checkoutView.delegate = self;

    [self.view addSubview:self.checkoutView];
    
}

- (void)stripeView:(STPView *)view withCard:(PKCard *)card isValid:(BOOL)valid
{
    // Enable save button if the Checkout is valid
    self.navigationItem.rightBarButtonItem.enabled = valid;
}

- (IBAction)save:(id)sender
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self.checkoutView createToken:^(STPToken *token, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if (error) {
            [self hasError:error];
        } else {
            [self hasToken:token];
        }
    }];
}

- (IBAction)cancel:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
}

- (void)hasError:(NSError *)error
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                      message:[error localizedDescription]
                                                     delegate:nil
                                            cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                            otherButtonTitles:nil];
    [message show];
}

- (void)hasToken:(STPToken *)token
{
    NSLog(@"Received token %@", token.tokenId);

    // TODO: Save token to standard defaults
    [_datasource createCustomer:token.tokenId success:^(NSString* customerId){
        [delegate dataLoaded:customerId];
    } error:nil];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

@end
