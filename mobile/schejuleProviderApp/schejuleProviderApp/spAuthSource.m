//
//  spAuth.m
//  sp
//
//  Created by Phillip Burch on 4/14/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "spAuthSource.h"

@implementation spAuthSource
@synthesize standardUserDefaults;

-(void) authenticate: (NSString *)userName password:(NSString *)password success:(void(^)(NSString * token))successBlock error: (void(^)(NSError * error)) errorBlock{
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *parameters = @{@"email": userName, @"password": password};
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:BaseUrl]];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    AFHTTPRequestOperation *op = [manager POST:@"/api/auth/login" parameters:parameters
    success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString * token = [responseObject objectForKey:@"token"];
        if (successBlock && token != nil) {
            successBlock([responseObject objectForKey:@"token"]);
        } else {
            if(errorBlock) {
                NSDictionary *userInfo = @{
                                           NSLocalizedDescriptionKey: NSLocalizedString(@"Oops", nil),
                                           NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Email or password was incorrect", nil)
                                           };
                errorBlock([NSError errorWithDomain:@"login" code:100 userInfo:userInfo]);
            }
        }
        [self requestFinished:responseObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(error);
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
    }];
    [op start];
}

-(void)signUp: (NSString *)email Phone:(NSString *)phone password: (NSString*)password success:(void(^)(NSString * token)) successBlock error: (void(^)(NSError * error)) errorBlock{
    standardUserDefaults = [NSUserDefaults standardUserDefaults];


    NSDictionary *parameters = @{@"phone": phone, @"password": password, @"email": email, @"isProvider": @"true"};
    
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:BaseUrl]];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    AFHTTPRequestOperation *op = [manager POST:@"/api/phone/signup" parameters:parameters
                                       success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                           NSString * token = [responseObject objectForKey:@"token"];
                                           if (successBlock && token != nil) {
                                               successBlock([responseObject objectForKey:@"token"]);
                                           } else {
                                               NSLog(@"%@", responseObject);
                                               if(errorBlock) {
//                                                   NSDictionary *errors = [responseObject objectForKey:@"errors"];
                                                   NSDictionary *userInfo = @{
                                                                              NSLocalizedDescriptionKey: NSLocalizedString(@"Oops", nil),
                                                                              NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Email address already in use.", nil)
                                                                              };
                                                   errorBlock([NSError errorWithDomain:@"login" code:100 userInfo:userInfo]);
                                               }
                                           }
                                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                           errorBlock(error);
                                           NSLog(@"Error: %@ ***** %@", operation.responseString, error);
                                       }];
    [op start];
}

-(void) requestFinished:(NSDictionary*)response
{
    [self.delegate authenticated:[response objectForKey:@"token"]];
}

@end
