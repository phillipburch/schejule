//
//  spResponseSerializer.m
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/6/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "spResponseSerializer.h"

@implementation spResponseSerializer

-(id)responseObjectForResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *__autoreleasing *)error
{
    NSDictionary * json = [super responseObjectForResponse:response data:data error:error];
    if([[json objectForKey:@"error"] isEqualToString: @"Bad Access token"]) {
        NSDictionary *userInfo = @{
           NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"Failed login", nil, @"schejule"),
           NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Bad login token", nil)
           };
        
        *error = [[NSError alloc] initWithDomain:@"login" code:401 userInfo:userInfo];

        return nil;
    }
        
    return json;
}

@end
