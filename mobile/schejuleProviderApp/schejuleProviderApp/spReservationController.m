//
//  spReservationController.m
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/3/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "spReservationController.h"

@interface spReservationController ()
{
    NSMutableArray *_objects;
    NSMutableArray *reservations;
    spReservationSource *_dataSource;
    spProviderSource *_providerDataSource;
    Boolean locationManuallySet;
    NSUserDefaults *_standardUserDefaults;
    CLLocationManager *_locationManager;
    Boolean _MapSet;
}
@end

@implementation spReservationController
@synthesize availableOutlet;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)awakeFromNib
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
    
    _dataSource = [[spReservationSource alloc] init];
    _providerDataSource = [[spProviderSource alloc] init];
    
    reservations = [[NSMutableArray alloc] init];
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    _standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if([_standardUserDefaults objectForKey:@"claimedReservation"] != nil) {
        [self performSegueWithIdentifier:@"toCurrent" sender:nil];
        return;
    }
    
    double latitude = [_standardUserDefaults doubleForKey:@"latitude"];
    double longitude = [_standardUserDefaults doubleForKey:@"longitude"];
    // Just converting to nsnumber to check for nil... odd?
    if(latitude != 0 && longitude != 0) {
        [_dataSource findAtLong:longitude andLat:latitude success:^(NSMutableArray * response) {
            reservations = response;
            [self.tableView reloadData];
        } error:nil];
    }
    
    if (!self.locationManager) {
        self.locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        [_locationManager startUpdatingLocation];
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%@", error);
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    if(locationManuallySet) return;
    CLLocation *location = [locations lastObject];
    //    NSLog(@"%@", locations);
    
    if (location.horizontalAccuracy <= manager.desiredAccuracy) {
        [self.locationManager startMonitoringSignificantLocationChanges];
        [self.locationManager stopUpdatingLocation];
        
        [_standardUserDefaults setDouble:location.coordinate.latitude forKey:@"latitude"];
        [_standardUserDefaults setDouble:location.coordinate.longitude forKey:@"longitude"];
        [_standardUserDefaults synchronize];
        
        [_dataSource findAtLong:location.coordinate.longitude andLat:location.coordinate.latitude success:^(NSMutableArray * response) {
            reservations = response;
            [self.tableView reloadData];
        } error:nil];
        
        [_providerDataSource updateLong:location.coordinate.longitude andLat:location.coordinate.latitude success:nil error:nil];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [reservations count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    spReservationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    spReservation * reservation = [reservations objectAtIndex:indexPath.row];
    cell.titleLable.text = reservation.address;
    cell.licensePlate.text = reservation.licensePlateNumber;
    [cell.carImage setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", BaseUrl, reservation.carPicture]] placeholderImage:[UIImage imageNamed:@"default"]];
    cell.toAgo.text = reservation.createdOn.timeAgoSinceNow;
    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"toDetail"]){
        spDetailController *detailController = [segue destinationViewController];

        NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];

        detailController.reservation = [reservations objectAtIndex:selectedIndexPath.row];
    }
}

- (IBAction)availableAction:(id)sender {
    __weak typeof(self) weakSelf = self;
    [_providerDataSource setAvailabe:YES success:^(){
        weakSelf.availableOutlet.title = @"Unavailable";
    } error:^(NSError *err){}];
}
@end
