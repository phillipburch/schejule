//
//  spReservationTableViewCell.h
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/3/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface spReservationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UIImageView *carImage;
@property (weak, nonatomic) IBOutlet UILabel *toAgo;
@property (weak, nonatomic) IBOutlet UILabel *licensePlate;

@end
