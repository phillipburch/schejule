//
//  spAppDelegate.h
//  spProviderApp
//
//  Created by Phillip Burch on 5/1/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface spAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
