//
//  spRegistrationController.h
//  sp
//
//  Created by Phillip Burch on 4/18/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "spAuthSource.h"

@interface spRegistrationController : UIViewController<spAuthSourceDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property NSUserDefaults *standardUserDefaults;
- (IBAction)signupAction:(id)sender;

@end
