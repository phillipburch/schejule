//
//  spCarDetailsController.m
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/5/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "spCarDetailsController.h"

@interface spCarDetailsController () {
    spReservationSource *_datasource;
    NSString * _reservationId;
    NSUserDefaults *_standardUserDefaults;
}
@end

@implementation spCarDetailsController
@synthesize licensePlateText, reservation;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    licensePlateText.delegate = self;
    _standardUserDefaults = [NSUserDefaults standardUserDefaults];
    _datasource = [[spReservationSource alloc] init];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //put code for store image
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    NSData *imageData = UIImageJPEGRepresentation(chosenImage, 0.5);

    [_datasource completeReservation:reservation._id withImageData:imageData success:^(NSString * reservationId){
        _reservationId = reservationId;
        
        [picker dismissViewControllerAnimated:YES completion:^{
            [_standardUserDefaults removeObjectForKey:@"claimedReservation"];
            [_standardUserDefaults synchronize];
            [self dismissViewControllerAnimated:NO completion:nil];
        }];
    } error:^(NSError * error) {
        [self alertOf:error];
    }];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)takePicture:(id)sender {
    self.picker = [[UIImagePickerController alloc] init];
    [self.picker setDelegate:self];
    self.picker.allowsEditing = YES;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"Take photo", @"Choose Existing", nil];
        [actionSheet showInView:self.view];
    } else {
        self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.picker
                           animated:YES completion:nil];
    }
}

- (IBAction)noPicture:(id)sender {
    [_datasource completeReservation:reservation._id success:^(NSString * reservationId){
        _reservationId = reservationId;
        
        [_standardUserDefaults removeObjectForKey:@"claimedReservation"];
        [_standardUserDefaults synchronize];
        [self dismissViewControllerAnimated:NO completion:nil];

    } error:^(NSError * error) {
        [self alertOf:error];
    }];
}

- (void) alertOf:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error.localizedDescription
                    message:error.localizedRecoverySuggestion
                   delegate:nil
          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    [alert show];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.picker
                           animated:YES completion:nil];

    } else if (buttonIndex == 1) {
        self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.picker
                           animated:YES completion:nil];

    }
    
}
@end
