//
//  spProviderPhotoController.m
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/8/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "spProviderPhotoController.h"

@interface spProviderPhotoController (){
    spProviderSource *_datasource;
    NSUserDefaults *_standardUserDefaults;
    NSData *_imageData;
}
@end

@implementation spProviderPhotoController
@synthesize profileImage, fullName;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _datasource = [[spProviderSource alloc]init];
    self.navigationController.navigationBar.hidden = NO;
    _standardUserDefaults = [NSUserDefaults standardUserDefaults];
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takeMyPicture:)];
    fullName.delegate = self;
    [singleTap setNumberOfTapsRequired:1];
    [profileImage addGestureRecognizer:singleTap];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.topItem.title = @"";
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //put code for store image
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    _imageData = UIImageJPEGRepresentation(chosenImage, 0.5);
    profileImage.image = [UIImage imageWithData:_imageData];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField.text.length > 0) {
        [self doneAction:textField];
        return YES;
    }
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)takeMyPicture:(id)sender {
    self.picker = [[UIImagePickerController alloc] init];
    [self.picker setDelegate:self];
    self.picker.allowsEditing = YES;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"Take photo", @"Choose Existing", nil];
        [actionSheet showInView:self.view];
    } else {
        self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.picker
                           animated:YES completion:nil];
    }
}

- (void) alertOf:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error.localizedDescription
                                                    message:error.localizedRecoverySuggestion
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    [alert show];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.picker
                           animated:YES completion:nil];
        
    } else if (buttonIndex == 1) {
        self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.picker
                           animated:YES completion:nil];
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doneAction:(id)sender {
    [_datasource createProvider:fullName.text success:^(){
        [_standardUserDefaults setBool:YES forKey:@"hasDetails"];
        [self performSegueWithIdentifier:@"toApp" sender:self];
    } error:nil];
}
@end
