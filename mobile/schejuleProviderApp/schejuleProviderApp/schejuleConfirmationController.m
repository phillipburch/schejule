//
//  schejuleOrderController.m
//  Schejule
//
//  Created by Phillip Burch on 3/25/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "schejuleConfirmationController.h"
#import "schejuleNewCustomerController.h"
#import "schejulePaymentSource.h"

@interface schejuleConfirmationController (){
    NSUserDefaults *_standardUserDefaults;
    schejulePaymentSource *_datasource;
}

@end

@implementation schejuleConfirmationController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _standardUserDefaults = [NSUserDefaults standardUserDefaults];
        _datasource = [[schejulePaymentSource alloc] init];
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    // This is actually bad... WE don't want to charge everytime we hit this controller
    [super viewWillAppear:animated];
    NSString * customerId = [_standardUserDefaults objectForKey:@"customerId"];
    if(customerId == nil) {
        [self performSegueWithIdentifier:@"toPayment" sender:self];
    } else{
        [_datasource chargeCustomer:customerId forReservation:self.reservationId success:nil error:nil];
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dataLoaded:(NSString * )customerId{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"toPayment"])
    {
        schejuleNewCustomerController *ccController = [segue destinationViewController];
        ccController.delegate = self;
        ccController.reservationId = self.reservationId;
    }
}
@end
