//
//  spReservationSource.h
//  sp
//
//  Created by Phillip Burch on 4/14/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"
#import "spResponseSerializer.h"
#import "spRequestSerializer.h"
#import "spReservation.h"

@class spReservationSource;

@protocol spReservationSourceDelegate <NSObject>
- (void)dataLoaded:(spReservationSource *)spReservationSource;
@end

@interface spReservationSource : NSObject
@property (strong) NSArray* totalItems;
@property (copy) NSString * searchQuery;
@property (strong) NSMutableData* responseData;
@property NSMutableArray* Results;
@property int page;
@property BOOL IsLoading;
@property BOOL CanLoadMore;

-(void) completeReservation: (NSString *)reservationId success:(void(^)(NSString * reservationId))successBlock error: (void(^)(NSError * error)) errorBlock;

-(void) completeReservation: (NSString *)reservationId withImageData:(NSData *)imageData success:(void(^)(NSString * reservationId))successBlock error: (void(^)(NSError * error)) errorBlock;

-(void) createReservation: (NSDictionary *)reservation withLicensePlate:(NSString *)plateNumber success:(void(^)(NSString * reservationId))successBlock error: (void(^)(NSError * error)) errorBlock;

- (id)initWithQuery:(NSString*)query;

-(void) createCustomer: (NSString *)paymentToken success:(void(^)(NSString * customer))successBlock error: (void(^)(NSError * error)) errorBlock;

-(void) findAtLong:(double)longitude andLat:(double)latitude success:(void(^)(NSMutableArray * providers)) successBlock error: (void(^)(NSError * error)) errorBlock;
@property (nonatomic, weak) IBOutlet id <spReservationSourceDelegate> delegate;
@end
