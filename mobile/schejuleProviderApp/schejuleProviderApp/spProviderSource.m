//
//  spProviderSource.m
//  sp
//
//  Created by Phillip Burch on 4/29/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "spProviderSource.h"
@implementation spProviderSource
@synthesize totalItems, searchQuery, responseData, Results, page, IsLoading, standardUserDefaults;


-(void) findAtLong:(double)longitude andLat:(double)latitude success:(void(^)(NSMutableArray * providers)) successBlock error: (void(^)(NSError * error)) errorBlock{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [spRequestSerializer serializer];
    manager.responseSerializer = [spResponseSerializer serializer];
    
    NSDictionary *parameters = @{@"longitude": [NSNumber numberWithDouble:longitude], @"latitude": [NSNumber numberWithFloat:latitude]};
    
    NSString *url = [NSString stringWithFormat:@"%1$@/api/providers", BaseUrl];
    
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if(successBlock) {
            successBlock([self requestFinished:responseObject]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

-(void) updateLong:(double)longitude andLat:(double)latitude success:(void(^)(NSMutableArray * providers)) successBlock error: (void(^)(NSError * error)) errorBlock{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *parameters = @{@"longitude": [NSNumber numberWithDouble:longitude], @"latitude": [NSNumber numberWithFloat:latitude], @"token": [standardUserDefaults stringForKey:@"AuthToken"]};
    
    NSString *url = [NSString stringWithFormat:@"%1$@/api/providers/me/location", BaseUrl];
    
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Error: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

-(void) createProvider: (NSString *)name withImageData:(NSData *)imageData success:(void(^)())successBlock error: (void(^)(NSError * error)) errorBlock {
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [standardUserDefaults stringForKey:@"AuthToken"];
    
    NSMutableDictionary *paramaters = [@{@"name": name} mutableCopy];
    [paramaters setObject:token forKey:@"token"];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:BaseUrl]];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    AFHTTPRequestOperation *op = [manager POST:@"/api/providers" parameters:paramaters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if(imageData != nil){
            [formData appendPartWithFileData:imageData name:@"photo" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
        }
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (successBlock) {
            successBlock();
        } else {
            if(errorBlock) {
                NSDictionary *userInfo = @{
                                           NSLocalizedDescriptionKey: NSLocalizedString(@"Oops", nil),
                                           NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Something went wrong.", nil)
                                           };
                errorBlock([NSError errorWithDomain:@"login" code:101 userInfo:userInfo]);
            }
        }
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(errorBlock) {
            NSDictionary *userInfo = @{
                                       NSLocalizedDescriptionKey: NSLocalizedString(@"Oops", nil),
                                       NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Something went wrong.", nil)
                                       };
            errorBlock([NSError errorWithDomain:@"Reservation" code:101 userInfo:userInfo]);
        }
    }];
    [op start];
}

-(void) createProvider: (NSString *)name success:(void(^)())successBlock error: (void(^)(NSError * error)) errorBlock {
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [standardUserDefaults stringForKey:@"AuthToken"];
    
    NSMutableDictionary *paramaters = [@{@"name": name} mutableCopy];
    [paramaters setObject:token forKey:@"token"];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:BaseUrl]];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    AFHTTPRequestOperation *op = [manager POST:@"/api/providers" parameters:paramaters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (successBlock) {
            successBlock();
        } else {
            if(errorBlock) {
                NSDictionary *userInfo = @{
                                           NSLocalizedDescriptionKey: NSLocalizedString(@"Oops", nil),
                                           NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Something went wrong.", nil)
                                           };
                errorBlock([NSError errorWithDomain:@"login" code:101 userInfo:userInfo]);
            }
        }
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(errorBlock) {
            NSDictionary *userInfo = @{
                                       NSLocalizedDescriptionKey: NSLocalizedString(@"Oops", nil),
                                       NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Something went wrong.", nil)
                                       };
            errorBlock([NSError errorWithDomain:@"Reservation" code:101 userInfo:userInfo]);
        }
    }];
    [op start];
}

-(void) setAvailabe:(BOOL)isAvailable success:(void(^)()) successBlock error: (void(^)(NSError * error)) errorBlock{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *token = [standardUserDefaults stringForKey:@"AuthToken"];
    
    NSDictionary *paramaters = @{@"token": token};

    
    NSString *url = [NSString stringWithFormat:@"%1$@/api/providers/me/markunavailable", BaseUrl];

    
    if(isAvailable == YES){
        url = [NSString stringWithFormat:@"%1$@/api/providers/me/markavailable", BaseUrl];
    }
    
    [manager POST:url parameters:paramaters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Error: %@", responseObject);
        successBlock();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        errorBlock(error);
    }];
}

-(NSMutableArray *) requestFinished:(NSDictionary*)items{
    NSArray *providers = [items objectForKey:@"providers"];
    NSMutableArray* myResults = [[NSMutableArray alloc] init];
    
    for (NSDictionary* c in providers) {
        spProvider *provider = [[spProvider alloc] init];
        
        provider.name = [c objectForKey:@"name"];
        provider._id = [c objectForKey:@"_id"];
        provider.longitude = [c objectForKey:@"longitude"];
        provider.latitude = [c objectForKey:@"latitude"];
        provider.user = [[c objectForKey:@"user"] objectForKey:@"name"];
        [myResults addObject:provider];
    }
    return myResults;
}

@end
