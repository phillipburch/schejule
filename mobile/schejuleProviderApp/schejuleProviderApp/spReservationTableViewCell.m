//
//  spReservationTableViewCell.m
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/3/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "spReservationTableViewCell.h"

@implementation spReservationTableViewCell
@synthesize titleLable, carImage, toAgo;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
