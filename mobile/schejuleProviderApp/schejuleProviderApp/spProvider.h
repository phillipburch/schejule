//
//  spProvider.h
//  sp
//
//  Created by Phillip Burch on 4/29/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface spProvider : NSObject
@property NSString* name;
@property NSString* user;
@property NSNumber *longitude;
@property NSNumber *latitude;
@property NSString *_id;
@end
