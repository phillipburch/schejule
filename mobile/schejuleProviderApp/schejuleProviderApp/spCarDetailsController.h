//
//  spCarDetailsController.h
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/5/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "spReservationSource.h"
#import "spReservation.h"

@interface spCarDetailsController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate>
- (IBAction)takePicture:(id)sender;
- (IBAction)noPicture:(id)sender;
@property (strong, nonatomic) spReservation *reservation;
@property (strong, nonatomic) UIImagePickerController *picker;
@property (weak, nonatomic) IBOutlet UITextField *licensePlateText;
@end
