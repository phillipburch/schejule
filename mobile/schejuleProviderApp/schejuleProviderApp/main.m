//
//  main.m
//  spProviderApp
//
//  Created by Phillip Burch on 5/1/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "spAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([spAppDelegate class]));
    }
}
