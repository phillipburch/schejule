//
//  schejuleSearchAddressController.h
//  Schejule
//
//  Created by Phillip Burch on 4/15/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class schejuleAddressController;

@protocol schejuleAddressControllerDelegate <NSObject>
- (void)addressChosen:(CLPlacemark *) placemark;
@end

@interface schejuleSearchAddressController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray* results;
@property (strong, nonatomic)   CLGeocoder * geoCoder;
@property (nonatomic, weak) IBOutlet id <schejuleAddressControllerDelegate> delegate;
- (IBAction)cancelButton:(id)sender;

@end
