//
//  spReservationController.h
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/3/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "spReservationSource.h"
#import "spReservationTableViewCell.h"
#import "spDetailController.h"
#import "spProviderSource.h"
#import "UIImageView+AFNetworking.h"
#import "DateTools.h"
@interface spReservationController : UITableViewController<CLLocationManagerDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;
- (IBAction)availableAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *availableOutlet;
@end
