//
//  spDetailController.h
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/4/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "spReservation.h"
#import <MapKit/MapKit.h>

@interface spDetailController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *licensePlate;
@property (weak, nonatomic) IBOutlet UIImageView *carImage;
- (IBAction)claimAction:(id)sender;
- (IBAction)directionsAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) spReservation * reservation;
@end
