//
//  schejuleCarDetailsController.h
//  Schejule
//
//  Created by Phillip Burch on 4/16/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "schejuleReservationSource.h"

@interface schejuleCarDetailsController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate>
- (IBAction)takePicture:(id)sender;
@property (strong, nonatomic) NSDictionary *reservationData;
@property (strong, nonatomic) UIImagePickerController *picker;
@property (weak, nonatomic) IBOutlet UITextField *licensePlateText;
@end
