//
//  spReservationSource.m
//  sp
//
//  Created by Phillip Burch on 4/14/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "spReservationSource.h"

@interface spReservationSource (){
    NSUserDefaults * standardUserDefaults;
}
@end

@implementation spReservationSource
@synthesize totalItems, searchQuery, responseData, Results, page, IsLoading;

- (id) init {
    if(self = [super init]) {
        standardUserDefaults = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

-(id)initWithQuery:(NSString *)query
{
    if (self = [super init]) {
        self.searchQuery = query;
    }
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
    Results = [[NSMutableArray alloc] init];
    return self;
    
}

- (AFHTTPRequestOperationManager *) getManager{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:BaseUrl]];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [spResponseSerializer serializer];
    return manager;
}

-(void) completeReservation: (NSString *)reservationId withImageData:(NSData *)imageData success:(void(^)(NSString * reservationId))successBlock error: (void(^)(NSError * error)) errorBlock {
    NSString *token = [standardUserDefaults stringForKey:@"AuthToken"];
    
    NSDictionary *parameters = @{@"token": token};
    
    AFHTTPRequestOperationManager *manager = [self getManager];
    NSString *url = [NSString stringWithFormat:@"/api/reservations/%@/complete", reservationId];
    
    AFHTTPRequestOperation *op = [manager POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imageData name:@"carImage" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        successBlock([[responseObject objectForKey:@"reservation"] objectForKey:@"_id"]);
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
    }];
    [op start];
}

-(void) completeReservation: (NSString *)reservationId success:(void(^)(NSString * reservationId))successBlock error: (void(^)(NSError * error)) errorBlock {
    NSString *token = [standardUserDefaults stringForKey:@"AuthToken"];

    NSDictionary *parameters = @{@"token": token};

    AFHTTPRequestOperationManager *manager = [self getManager];
    NSString *url = [NSString stringWithFormat:@"/api/reservations/%@/complete", reservationId];
    
    AFHTTPRequestOperation *op = [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        successBlock([[responseObject objectForKey:@"reservation"] objectForKey:@"_id"]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
    }];
    [op start];
}

-(void) createReservation: (NSDictionary *)reservation withLicensePlate:(NSString *)plateNumber success:(void(^)(NSString * reservationId))successBlock error: (void(^)(NSError * error)) errorBlock {
    NSString *token = [standardUserDefaults stringForKey:@"AuthToken"];
    NSMutableDictionary* parameters = [reservation mutableCopy];

    [parameters setObject:token forKey:@"token"];
    [parameters setObject:plateNumber forKey:@"license"];
    
    AFHTTPRequestOperationManager *manager = [self getManager];
    AFHTTPRequestOperation *op = [manager POST:@"/api/reservations" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(successBlock) {
            successBlock([responseObject objectForKey:@"_id"]);
        }
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
    }];
    
    [op start];
}


-(void) createCustomer: (NSString *)paymentToken success:(void(^)(NSString * customer))successBlock error: (void(^)(NSError * error)) errorBlock {

    NSString *token = [standardUserDefaults stringForKey:@"AuthToken"];
    NSMutableDictionary* parameters = [@{@"token": token, @"paymentToken": paymentToken} mutableCopy];
    
    AFHTTPRequestOperationManager *manager = [self getManager];
    
    NSString * url = [NSString stringWithFormat:@"/api/customers"];
    AFHTTPRequestOperation *op = [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString * customerId = [responseObject objectForKey:@"id"];
        [standardUserDefaults setValue:customerId forKey:@"customerId"];
        [standardUserDefaults synchronize];
        successBlock(customerId);
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
    }];
    
    [op start];
}

-(void) findAtLong:(double)longitude andLat:(double)latitude success:(void(^)(NSMutableArray * providers)) successBlock error: (void(^)(NSError * error)) errorBlock{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [spResponseSerializer serializer];
    manager.requestSerializer = [spRequestSerializer serializer];
    
    NSDictionary *parameters = @{@"longitude": [NSNumber numberWithDouble:longitude], @"latitude": [NSNumber numberWithFloat:latitude], @"claimed":@"false"};
    
    NSString *url = [NSString stringWithFormat:@"%1$@/api/reservations", BaseUrl];
    
    [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if(successBlock) {
            successBlock([self requestFinished:responseObject]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

-(NSMutableArray *) requestFinished:(NSDictionary*)items{
    NSArray *reservations = [items objectForKey:@"reservations"];
    NSMutableArray* myResults = [[NSMutableArray alloc] init];
    NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    
	[rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"];
	[rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    for (NSDictionary* c in reservations) {
        spReservation *reservation = [[spReservation alloc] init];
        
        reservation.eta = [c objectForKey:@"eta"];
        reservation.price = [c objectForKey:@"price"];
        reservation._id = [c objectForKey:@"_id"];
        reservation.carPicture = [c objectForKey:@"carImage"];
        reservation.licensePlateNumber = [c objectForKey:@"license"];
        reservation.address = [c objectForKey:@"address"];
        reservation.latitude = [[c objectForKey:@"latitude"] doubleValue];
        reservation.longitude = [[c objectForKey:@"longitude"] doubleValue];
        reservation.createdOn = [rfc3339DateFormatter dateFromString:[c objectForKey:@"createdOn"] ];
        
        if([c objectForKey:@"user"] != (id)[NSNull null])
            reservation.userName = [[c objectForKey:@"user"] objectForKey:@"email"];

        [myResults addObject:reservation];
    }
    return myResults;
}

@end
