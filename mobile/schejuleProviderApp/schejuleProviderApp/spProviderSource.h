//
//  spProviderSource.h
//  sp
//
//  Created by Phillip Burch on 4/29/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"
#import "spProvider.h"
#import "spRequestSerializer.h"
#import "spResponseSerializer.h"

@interface spProviderSource : NSObject
@property (strong) NSArray* totalItems;
@property (copy) NSString * searchQuery;
@property (strong) NSMutableData* responseData;
@property NSMutableArray* Results;
@property int page;
@property BOOL IsLoading;
@property BOOL CanLoadMore;
-(void) createProvider: (NSString *)name withImageData:(NSData *)imageData success:(void(^)())successBlock error: (void(^)(NSError * error)) errorBlock;

-(void) createProvider: (NSString *)name success:(void(^)())successBlock error: (void(^)(NSError * error)) errorBlock;

-(void) findAtLong:(double)longitude andLat:(double)latitude success:(void(^)(NSMutableArray * providers)) successBlock error: (void(^)(NSError * error)) errorBlock ;

-(void) updateLong:(double)longitude andLat:(double)latitude success:(void(^)(NSMutableArray * providers)) successBlock error: (void(^)(NSError * error)) errorBlock;

-(void) setAvailabe:(BOOL)isAvailable success:(void(^)()) successBlock error: (void(^)(NSError * error)) errorBlock;
@property NSUserDefaults *standardUserDefaults;
@end
