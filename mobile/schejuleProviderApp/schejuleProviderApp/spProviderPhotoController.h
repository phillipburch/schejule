//
//  spProviderPhotoController.h
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/8/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "spProviderSource.h"

@interface spProviderPhotoController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate>
- (void)takeMyPicture:(id)sender;
@property (strong, nonatomic) UIImagePickerController *picker;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
- (IBAction)doneAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *fullName;

@end
