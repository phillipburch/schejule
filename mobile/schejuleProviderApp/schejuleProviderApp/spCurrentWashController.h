//
//  spCurrentWashController.h
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/5/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "spReservation.h"
#import "spCarDetailsController.h"
#import "UIImageView+AFNetworking.h"

@interface spCurrentWashController : UIViewController
- (IBAction)directionsAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *currentImage;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) spReservation * reservation;
@property (weak, nonatomic) IBOutlet UILabel *licensePlate;
@end
