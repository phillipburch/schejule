//
//  spRequestSerializer.m
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/6/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "spRequestSerializer.h"

@interface spRequestSerializer () {
    NSUserDefaults *_standardUserDefaults;
}
@end

@implementation spRequestSerializer
- (NSURLRequest *)requestBySerializingRequest:(NSURLRequest *)request
                               withParameters:(id)parameters
                                        error:(NSError *__autoreleasing *)error
{
 
    _standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [_standardUserDefaults stringForKey:@"AuthToken"];
    
    NSMutableDictionary * params = [parameters mutableCopy];
    
    [params setValue:token forKey:@"token"];
    
    return [super requestBySerializingRequest:request withParameters:params error:error];
}
@end
