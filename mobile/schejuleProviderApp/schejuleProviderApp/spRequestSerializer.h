//
//  spRequestSerializer.h
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/6/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFURLRequestSerialization.h"

@interface spRequestSerializer : AFJSONRequestSerializer

@end
