//
//  spDetailController.m
//  schejuleProviderApp
//
//  Created by Phillip Burch on 5/4/14.
//  Copyright (c) 2014 Phillip Burch. All rights reserved.
//

#import "spDetailController.h"
#import "UIImageView+AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "DateTools.h"

@interface spDetailController (){
    NSUserDefaults *standardUserDefaults;
}

@end

@implementation spDetailController
@synthesize addressLabel, licensePlate, carImage, reservation, timeLabel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    addressLabel.text = reservation.address;
    licensePlate.text = reservation.licensePlateNumber;
    
    timeLabel.text = reservation.createdOn.timeAgoSinceNow;
    [carImage setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", BaseUrl, reservation.carPicture]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)claimAction:(id)sender {
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:BaseUrl]];
    NSDictionary *parameters = @{@"token": [standardUserDefaults stringForKey:@"AuthToken"]};
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *url = [NSString stringWithFormat:@"/api/reservations/%@/claim", reservation._id];
    
    AFHTTPRequestOperation *op = [manager GET:url parameters:parameters
                                       success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                           NSLog(@"%@", responseObject);
                                           NSData *rencoded = [NSKeyedArchiver archivedDataWithRootObject:reservation];

                                           [standardUserDefaults setObject:rencoded forKey:@"claimedReservation"];
                                           [standardUserDefaults synchronize];
                                           [self performSegueWithIdentifier:@"toCurrent" sender:nil];
                                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                           NSLog(@"Error: %@ ***** %@", operation.responseString, error);
                                       }];
    [op start];
    
}

- (IBAction)directionsAction:(id)sender {
    // Create an MKMapItem to pass to the Maps app
    CLLocationCoordinate2D coordinate =
    CLLocationCoordinate2DMake(reservation.latitude, reservation.longitude);
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                   addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:@"Carwash location"];
    
    // Set the directions mode to "Walking"
    // Can use MKLaunchOptionsDirectionsModeDriving instead
    NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
    // Get the "Current User Location" MKMapItem
    MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
    // Pass the current location and destination map items to the Maps app
    // Set the direction mode in the launchOptions dictionary
    [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem]
                   launchOptions:launchOptions];
}
@end
