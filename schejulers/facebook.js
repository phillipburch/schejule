// TODO: Create a worker to get all facebook friends and stick them in database.
// TODO: Create a worker that checks to see if other friends are on facebook.
var mongoose = require('mongoose'),
  kue = require('schejulekue'),
  jobs = kue.createQueue(),
  nodefn = require('when/node'),
  when = require('when');

module.exports = {
  friends: function (options, model) {
    jobs.create('facebook:friends', {
      title: "Populate facebook friends for " + model.email,
      model: model
    }).attempts(5).save();
  }
};
