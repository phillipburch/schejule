var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  kue = require('schejulekue'),
  jobs = kue.createQueue(),
  nodefn = require('when/node'),
  schejulers = require('./'),
  Reservation = mongoose.model('Reservation'),
  when = require('when');

module.exports = {
  new: function (options, model) {
    jobs.create('email', {
      title: "Email to " + options.to + " " + options.subject,
      subject: options.subject,
      to: options.to,
      from: options.from,
      template: options.template,
      model: model
    }).attempts(5).save();
  }
};
