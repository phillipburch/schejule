module.exports = {
  reservation: require('./reservation'),
  facebook: require('./facebook'),
  email: require('./email')
};
