var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  kue = require('schejulekue'),
  jobs = kue.createQueue(),
  nodefn = require('when/node'),
  emailSchejuler = require('./email'),
  Reservation = mongoose.model('Reservation'),
  when = require('when');

module.exports = {
  // Sends an email and creates a job to find nearby providers
  new: function (reservation) {
    emailSchejuler.new({
      to: 'Phillipgburch@gmail.com',
      from: 'phillip@lastvine.com',
      template: 'newReservation',
      subject: 'Carwash schejuled!'
    }, reservation);

    Reservation.populate(reservation, {path: 'user'}, function(err, reservation) {
      console.log(reservation);
      jobs.create('reservation', {
        title: 'Reservation for ' + reservation.user.name,
        model: reservation
      }).save();
    })
  },

  completed: function(reservation) {
    emailSchejuler.new({
      to: 'Phillipgburch@gmail.com',
      from: 'phillip@lastvine.com',
      template: 'completedReservation',
    }, reservation);

    Reservation.populate(reservation, {path: 'user'}, function(err, reservation) {
      jobs.create('charge', {
        title: 'Reservation for ' + reservation.user.name,
        model: reservation
      }).save();
    })

  }
};
