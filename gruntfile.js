var uuid = require('node-uuid')
  , service = require('./server/service/buildconfig')
  , crypto = require('crypto')
  , path = require('path')
  , fs = require('fs');

/*global module:false*/
module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-node-inspector');
  grunt.loadNpmTasks('grunt-replace');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-ember-templates');

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    meta: {
      banner: '/* Copyrite <%= pkg.name %> */'
    },

    clean: ['server/public/css/**', 'server/public/js/**', 'server/public/fonts/**'],

    copy: {
      fonts: {
        expand: true,
        cwd: 'client/assets/fonts',
        src: '**/*',
        dest: 'server/public/fonts'
      }
    },

    'node-inspector': {
      app: {}
    },

    nodemon: {
      dev: {
        script: 'server/app.js',
        options: {
          nodeArgs: ['--debug'],
          watch: ['server', 'workers', 'schejulers'],
          ignore: ['public/**', '*.jade', 'temp/**', 'resources**']
        }
      }
    },

    emberTemplates: {
      compile: {
        options: {
          templateBasePath: /client\/templates\//
        },
        files: { // TODO: md5
          "server/public/js/templates-<%= meta.buildId %>.js": ["client/templates/**/*.hbs"]
        }
      }
    },

    uglify: {
      vendor: {
          src: ['<banner:meta.banner>'
            ,  'client/assets/vendor/jquery.js'
            , 'client/assets/vendor/handlebars-1.1.2.js'
            , 'client/assets/vendor/ember-1.5.0.js'
            , 'client/assets/vendor/ember-data.js'
            , 'client/assets/vendor/form.jquery.js'
            , 'client/assets/vendor/moment-2.6.0.js'
          ],
          dest: 'server/public/js/app-vendor.js'
      },

      app: {
        options: {
          mangle: false,
          compress: false,
          beautify: true,
          sourceMap: true,
          sourceMapIncludeSources: true,
          sourceMapName: 'server/public/js/source-map.js',
          banner: '/*! <%= pkg.name %> ' +
            '<%= grunt.template.today("yyyy-mm-dd") %> */'
        },
        src: ['<banner:meta.banner>',
          'client/**/*.js',
          '!client/assets/**/*'
        ],
        dest: 'server/public/js/combined-app.js',
        md5BasePath: 'server/public/js/app.min.'
      }
    },

    replace: {
      dist: {
        options: {
          variables: {
            'buildId': '<%= meta.buildId %>'
          }
        },
        files: [
          {
            src: ['server/views/layout_src.jade'],
            dest: 'server/views/layout.jade'
          }
        ]
      }
    },

    less: {
      app: {
        options: {
          yuicompress: true
        },
        src: "client/assets/less/imports.less",
        dest: "server/public/css/combined-app.css",
        md5BasePath: 'server/public/css/app.min.'
      }
    },

    jade: {
      frontend: {
        options: {
          compileDebug: false,
          namespace: 'App.templates',
          client: true,
          data: {
            debug: false
          },
          processName: function(str) { return str.match(/^server\/views\/shared\/(.*)\.jade$/)[1]; }
        },
        files: {
          'server/public/js/templates-<%= meta.buildId %>.js': 'server/views/shared/*.jade'
        }
      }
    },

    watch: {
      options: {
        livereload:true,
        nospawn: true
      },
      javascript: {
        files: ['client/**/*.js', '!client/assets/**/*' ],
        tasks: ['UpdateJS']
      },
      layout_src: {
        files: 'server/views/layout_src.jade',
        tasks: ['UpdateLayout']
      },
      css: {
        files: 'client/**/*.less',
        tasks: ['UpdateCSS']
      },
      jade: {
        files: 'server/views/shared/*.jade',
        tasks: ['UpdateJade']
      },
      handlebars: {
        files: 'client/**/*.hbs',
        tasks: ['UpdateEmber']
      }
    },

    concurrent: {
      watchers: {
        tasks: ['nodemon', 'watch'],
        options: {
          logConcurrentOutput: true
        }
      },
      compilers: {
        tasks: ['jade:frontend', 'uglify', 'less'],
        options: {
          logConcurrentOutput: true
        }
      }
    }
  });

  grunt.registerTask('UpdateLayout', function() {
    var buildConfig = service.getConfig();
    grunt.config.set('replace.dist.options.variables', buildConfig);
    grunt.task.run(['replace']);
  });

  grunt.registerTask('UpdateJS', function() {
    grunt.task.run(['uglify:app', '_md5-js-assets', 'replace', 'copy']);
  });

  grunt.registerTask('UpdateCSS', function() {
    grunt.task.run(['less', '_md5-css-assets', 'replace', 'copy']);
  });

  grunt.registerTask('UpdateJade', function() { //TODO: Probably not necessary remove jade dependency
    var buildConfig = service.getConfig();
    buildConfig.jade = uuid.v4();
    grunt.config.set('replace.dist.options.variables', buildConfig);

    grunt.config.set('meta.buildId', buildConfig.jade);
    grunt.task.run(['jade:frontend', 'replace']);
    service.saveConfig(buildConfig);
  });

  grunt.registerTask('UpdateEmber', function() {
    var buildConfig = service.getConfig();
    buildConfig.jade = uuid.v4();
    grunt.config.set('replace.dist.options.variables', buildConfig);

    grunt.config.set('meta.buildId', buildConfig.jade);
    grunt.task.run(['emberTemplates', 'replace']);
    service.saveConfig(buildConfig);
  });

  grunt.registerTask('_md5-js-assets', function() {
    var minifiedJS = grunt.config.get('uglify.app.dest'),
      buffer = fs.readFileSync(minifiedJS),
      resolve = path.resolve,
      md5 = crypto.createHash('md5')

    md5.update(buffer);
    var hash = md5.digest('hex');
    var md5Path = grunt.config.get('uglify.app.md5BasePath') + hash + '.js';

    fs.renameSync(resolve(__dirname, minifiedJS), resolve(__dirname, md5Path));
    grunt.log.ok("File: " + md5Path + " created.")

    var buildConfig = service.getConfig();
    buildConfig.md5JS = hash;
    grunt.config.set('replace.dist.options.variables', buildConfig);

    fs.writeFileSync('currentbuild.json', JSON.stringify(buildConfig));
  })

  grunt.registerTask('_md5-css-assets', function() {
    var minifiedCSS = grunt.config.get('less.app.dest'),
      buffer = fs.readFileSync(minifiedCSS),
      md5 = crypto.createHash('md5'),
      resolve = path.resolve;

    md5.update(buffer);
    var hash = md5.digest('hex');
    var md5Path = grunt.config.get('less.app.md5BasePath') + hash + '.css';

    fs.renameSync(resolve(__dirname, minifiedCSS), resolve(__dirname, md5Path));
    grunt.log.ok("File: " + md5Path + " created.")

    var buildConfig = service.getConfig();
    buildConfig.md5CSS = hash;
    grunt.config.set('replace.dist.options.variables', buildConfig);
    fs.writeFileSync('currentbuild.json', JSON.stringify(buildConfig));
  })

  grunt.registerTask('default', 'Default dev task', function() {
    var buildConfig = service.getConfig();
    var buildid = uuid.v4();

    buildConfig.jade = buildid;

    grunt.config.set('meta.buildId', buildid);
    grunt.config.set('replace.dist.options.variables', buildConfig);

    fs.writeFileSync('currentbuild.json', JSON.stringify(buildConfig));
    grunt.task.run(['clean', 'emberTemplates', 'uglify', '_md5-js-assets', 'less' , '_md5-css-assets', 'replace', 'copy','concurrent:watchers']);
  });
};
